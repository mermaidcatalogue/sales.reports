import zope.interface
from plone.namedfile.file import NamedFile
from sales.reports import _


def isFileTypeCSV(value):
    """
    check if uploading file is csv
    """
    if isinstance(value, NamedFile):
        # import pdb;pdb.set_trace()
        if value.contentType not in ['text/csv', 'text/comma-separated-values']:
            raise zope.interface.Invalid(_(u"Only CSV files are allowed to be uploaded, but found %s" % str(value.contentType)))
    return True