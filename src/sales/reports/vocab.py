
import inspect

from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
import datetime
from zope.component.hooks import getSite
from zope.globalrequest import getRequest
from Acquisition import aq_inner, aq_parent
from Products.CMFCore.interfaces import ISiteRoot
from plone import api
from sales.reports import _
from plone.app.vocabularies.catalog import CatalogVocabulary
from zope.interface import implementer
from zope.schema.interfaces import IVocabularyFactory
from sales.reports.browser.utils import getRelatedObjectInfo
from zope.globalrequest import getRequest
from plone.app.uuid.utils import uuidToObject
import time

from five import grok
from zope.schema.interfaces import IContextSourceBinder


from Acquisition import aq_get
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import safe_unicode
from zope.i18n import translate
from zope.i18nmessageid import MessageFactory
from zope.interface import implementer
from zope.schema.interfaces import IVocabularyFactory
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from zope.site.hooks import getSite




def make_terms(items):
    """ Create zope.schema terms for vocab from tuples """
    terms = [SimpleTerm(value=pair[0], token=pair[0], title=pair[1]) for pair in items]
    return terms

def list_products(context):
    """  Get id -> name list of products the logged in user is allowed to see.
    """
    brains = api.content.find(portal_type='Product', 
                            sort_on="sortable_title")

    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_expense_types(context):
    """  Get id -> name list of expense types the logged in user is allowed to see.
    """
    request = getRequest()
    expense_type = request.get('expense_type', '')
    if expense_type:
    
        brains = api.content.find(portal_type='ExpenseType', 
                                sort_on="sortable_title",
                                UID = expense_type)

    else:
        brains = api.content.find(portal_type='ExpenseType', 
                                sort_on="sortable_title")

    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_expense_types_a(context):
    """  Get id -> name list of expense types the logged in user is allowed to see.
    """
    request = getRequest()
    expense_type = request.get('expense_type', '')
    if expense_type:
        obj = uuidToObject(expense_type)
    
        brains = api.content.find(obj, portal_type='ExpenseTypeA', 
                                sort_on="sortable_title")

    else:
        brains = api.content.find(portal_type='ExpenseTypeA', 
                                sort_on="sortable_title")

    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_expense_types_b(context):
    """  Get id -> name list of expense types the logged in user is allowed to see.
    """
    request = getRequest()
    expense_type = request.get('expense_type', '')
    if expense_type:
        obj = uuidToObject(expense_type)
    
        brains = api.content.find(obj, portal_type='ExpenseTypeB', 
                                sort_on="sortable_title")

    else:
        brains = api.content.find(portal_type='ExpenseTypeB', 
                                sort_on="sortable_title")


    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_expense_types_c(context):
    """  Get id -> name list of expense types the logged in user is allowed to see.
    """
    request = getRequest()
    expense_type = request.get('expense_type', '')
    if expense_type:
        obj = uuidToObject(expense_type)
    
        brains = api.content.find(obj, portal_type='ExpenseTypeC', 
                                sort_on="sortable_title")

    else:
        brains = api.content.find(portal_type='ExpenseTypeC', 
                                sort_on="sortable_title")


    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_expense_locations(context):
    """  Get id -> name list of expense location the logged in user is allowed to see.
    """
    brains = api.content.find(portal_type='ExpenseLocation', 
                            sort_on="sortable_title")

    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_clients(context):
    """  Get id -> name list of clients the logged in user is allowed to see.
    """
    brains = api.content.find(portal_type='Client', 
                            sort_on="sortable_title")

    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)

def list_collectors(context):
    """  Get id -> name list of collectors the logged in user is allowed to see.
    """
    brains = api.content.find(portal_type='Collector', 
                            sort_on="sortable_title")

    # Create a list of tuples (UID, Title) of results
    result = [(brain['UID'], brain['Title'])for brain in brains]
    # Convert tuples to SimpleTerm objects
    terms = make_terms(result)

    return SimpleVocabulary(terms)


def list_disposals(context):
    """  Get id -> name list of clients the logged in user is allowed to see.
    """
    start_time = time.time()
    # with_balance = {'query': 1, 'range': 'min'}
    # request = getRequest()
    # query = {}
    # client = request.get('client', '')
    # if client:
    #     query['client'] = client

    # if '++add++Collection' in request.getURL():
        
    #     brains = api.content.find(portal_type='Disposal', 
    #                             sort_on="client", 
    #                             payment_type="TERMS",
    #                             unassigned_collection_amount= with_balance, **query)
    # else:  
    #     brains = api.content.find(portal_type='Disposal', 
    #                             payment_type="TERMS",
    #                             sort_on="client", **query)
        
    # Create a list of tuples (UID, Title) of results
    result = []
    for brain in brains:
        obj = brain.getObject()
        if obj.client:
            client = getRelatedObjectInfo(obj.client)
            title = "{} - {} - Php {} - Php {}".format(
                client['title'],
                brain.reference_no,
                brain.disposal_total,
                brain.unassigned_collection_amount)
            result.append((brain['UID'], title))


    # # Convert tuples to SimpleTerm objects
    # terms = make_terms(result)
    print "list_disposals {}".format(time.time()-start_time)
    #return SimpleVocabulary(terms)
    return SimpleVocabulary(
        [SimpleTerm(value=u'cash', title=_(u'Cash')),
         SimpleTerm(value=u'check', title=_(u'Check')),]
        )

# @implementer(IVocabularyFactory)
# class DisposalsVocabulary(object):

#     def __init__(self, key):
#         self.key = key

#     def __call__(self, context):
#         items = []
#         import pdb; pdb.set_trace()
#         brains = api.content.find(portal_type='Disposal', 
#                         payment_type="TERMS",
#                         sort_on="client")
#         if brains is not None:
#             for brain in brains:
#                 title = "{} - Php {} - Php {}".format(
#                     brain.reference_no,
#                     brain.disposal_total,
#                     brain.unassigned_collection_amount)
#                 items.append(SimpleTerm(brain['UID'], brain['UID'], safe_unicode(title)))

#         return SimpleVocabulary(items)

# DisposalsVocabularyFactory = DisposalsVocabulary()



@implementer(IVocabularyFactory)
class DisposalsVocabulary(object):

    def __call__(self, context):
        with_balance = {'query': 1, 'range': 'min'}

        site = getSite()
        catalog = getToolByName(site, 'portal_catalog', None)
        request = aq_get(catalog, 'REQUEST', None)

        client = request.get('client', '')
        query = {}
        if client:
            query['client'] = client

        if '++add++Collection' in request.getURL():
            brains = api.content.find(portal_type='Disposal', 
                                    sort_on="client", 
                                    payment_type="TERMS",
                                    unassigned_collection_amount= with_balance, **query)
        else:  
            brains = api.content.find(portal_type='Disposal', 
                                    payment_type="TERMS",
                                    sort_on="client", **query)

        items = []
        if brains is not None:
            for brain in brains:
                # obj = brain.getObject()
                # client = ''
                # if obj.client:
                #     client = getRelatedObjectInfo(obj.client)['title']
                title = "{} - Php {} - Php {}".format(
                    brain.reference_no,
                    brain.disposal_total,
                    brain.unassigned_collection_amount)
                items.append(SimpleTerm(brain['UID'], brain['UID'], title))
        return SimpleVocabulary(items)

DisposalsVocabularyFactory = DisposalsVocabulary()
