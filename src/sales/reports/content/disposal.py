# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from z3c.form.browser.checkbox import SingleCheckBoxFieldWidget
from plone import api
from sales.reports.browser.utils import getRelatedObjectInfo
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.dexterity.browser import add
from plone.dexterity.browser.edit import DefaultEditForm, DefaultEditView
from plone.dexterity.browser import edit
import time
import datetime


disposal_types = SimpleVocabulary(
    [SimpleTerm(value=u'SAMPLE', title=_(u'Sample')),
     SimpleTerm(value=u'ADJUSTMENT', title=_(u'Adjustment')),]
    )

payment_types = SimpleVocabulary(
    [SimpleTerm(value=u'COD', title=_(u'COD (Cash on Date)')),
     SimpleTerm(value=u'TERMS', title=_(u'TERMS')),]
    )

collectors = SimpleVocabulary(
    [SimpleTerm(value=u'Allan', title=_(u'Allan')),
     SimpleTerm(value=u'Collector 1', title=_(u'Collector 1')),]
    )

class IProductsList(Interface):
    product_uid = schema.Choice(title=_(u'Products'),
        vocabulary="list_products",
        required=False)
    kilos = schema.Float(
        title=_(u"Kilos"),
        required=True,
    )

    price = schema.Float(
        title=_(u"Price"),
        required=False,
    )


class IDisposal(Interface):

    reference_no = schema.TextLine(
        title=_(u'OR No.'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    client = schema.Choice(title=_(u'Client'),
        vocabulary="list_clients",
        required=True)

    client_walk_in = schema.TextLine(
        title=_(u'Walk In Client Name'),
        required=False,
    )

    directives.mode(withholding='hidden')
    withholding = schema.Float(
        title=_(u'Withholding (%)'),
        required=False,
    )

    directives.widget('input_products', DataGridFieldFactory)
    input_products = schema.List(title=_(u'Products'),
                              value_type=DictRow(title=u'Products, Kilos and Prices', schema=IProductsList),
                              required=True)

    directives.mode(disposal_type='hidden')
    disposal_type = schema.Choice(
            title=_(u"Disposal Type"),
            vocabulary=disposal_types,
            required=False,
        )

    payment_type = schema.Choice(
        title=_(u"Payment Type"),
        vocabulary=payment_types,
        required=True,
    )

    collector = schema.Choice(
        title=_(u'Collector'),
        vocabulary='list_collectors',
        required=False,
    )

    # directives.widget(office=SingleCheckBoxFieldWidget)
    # office = schema.Bool(
    #         title=_(u"Office"),
    #         default=True,
    #     )

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )



    @invariant
    def validate_disposal(data):
        already_added = set()
        for product in data.input_products:
            product_uid = product['product_uid']
            if product_uid:
                # prevent multiple assignments of same product
                if product_uid in already_added:
                    raise Invalid(_(u'Duplicate product. Please double check.'))

                already_added.add(product_uid)

        if data.payment_type == 'TERMS':
            if not data.collector:
                raise Invalid(_(u'Please input a collector.'))



class Disposal(Item):
    # Make sure Item's accessors don't take precedence
    # Add your class methods and properties here
    def get_object_uids(self):
        return [row['product_uid'] for row in (self.input_products or [])]

    def get_object_kilos(self):
        return dict(
            (row['product_uid'], row['kilos'])
            for row in (self.input_products or [])
        )

    def get_object_price(self):
        return dict(
            (row['product_uid'], row['price'])
            for row in (self.input_products or [])
        )

    def get_disposal_total(self):
        start_time = time.time()
        total = sum([p['price'] * p['kilos'] for p in (self.input_products or []) if p['price']])
        print "get_disposal_total {}".format(time.time()- start_time)
        # if self.withholding:
        #     total = total + (total * (float(self.withholding)/100))
        return round(total) 


    def update_unassigned_collection_amount(self):
        start_time = time.time()
        v = self.get_disposal_total()
        # for b in api.content.find(portal_type=['ManagementGoalSheet', 'CostCenterSheet'],
        #                           collection_disposals=[self.UID()]):
        for b in api.content.find(portal_type=['Collection'], collection_disposals=[self.UID()]):
            ob = b.getObject()
            if not ob.bounce_check:
                print "disposal {}".format(b)
                v -= ob.get_collection_amount().get(self.UID()) or 0

        # if v < 0:
        #     raise ValueError('negative unassigned cost center percentage of participation for {}'.format(self.absolute_url()))
        self._unassigned_collection_amount = v
        self.reindexObject(idxs=['unassigned_collection_amount'])
        print "update_unassigned_collection_amount {}".format(time.time()- start_time)

    def get_unassigned_collection_amount(self):
        try:
            #delete, add
            return self._unassigned_collection_amount
        except AttributeError:
            self.update_unassigned_collection_amount()
            return self._unassigned_collection_amount

    def get_client_name(self):
        return getRelatedObjectInfo(self.client, disposal=True)


@indexer(IDisposal)
def product_uids(obj):
    return obj.get_object_uids()

@indexer(IDisposal)
def product_kilos(obj):
    return obj.get_object_kilos()

@indexer(IDisposal)
def product_price(obj):
    return obj.get_object_price()

@indexer(IDisposal)
def disposal_total(obj):
    return obj.get_disposal_total()

@indexer(IDisposal)
def unassigned_collection_amount(obj):
    return obj.get_unassigned_collection_amount()

def on_disposal_update(doc, event):
    start_time = time.time()
    if hasattr(event, 'descriptions') and event.descriptions:
        for d in event.descriptions:
            if d.interface is IDisposal and ('input_products' in d.attributes):
                doc.update_unassigned_collection_amount()
    print "on_disposal_update {}".format(time.time()- start_time)



class AddForm(add.DefaultAddForm):
    template = ViewPageTemplateFile('templates/disposal_add_form.pt')


class AddView(add.DefaultAddView):
    form = AddForm


class EditForm(edit.DefaultEditForm):
    template = ViewPageTemplateFile('templates/disposal_edit_form.pt')