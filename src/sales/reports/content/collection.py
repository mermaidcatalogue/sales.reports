# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from z3c.relationfield.schema import RelationChoice
from z3c.relationfield.schema import RelationList
from sales.reports.content.disposal import IDisposal
from plone import api

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.dexterity.browser import add
from plone.dexterity.browser.edit import DefaultEditForm, DefaultEditView
import z3c.form

from sales.reports.browser.utils import getRelatedObjectInfo
from plone.dexterity.browser import edit
from z3c.form.browser.checkbox import SingleCheckBoxFieldWidget
import time
from plone.directives import form
import json
import datetime

# from sales.reports.vocab import DisposalsVocabularyFactory



collection_types = SimpleVocabulary(
    [SimpleTerm(value=u'cash', title=_(u'Cash')),
     SimpleTerm(value=u'check', title=_(u'Check')),]
    )

class ICollectionDisposal(Interface):
    collection_disposal = schema.Choice(
        title=_(u'Client - Reference No. - Total Amount - Running Balance'),
        vocabulary='list_disposals_factory',
        required=False)

    collection_amount = schema.Float(
        title=_(u"Amount"),
        required=False,
    )

# class ITableRowSchema(form.Schema):


#     independent = schema.Bool(
#         title=u"Check me",
#         description=u"Do not take part to the decision tree",
#         required=False)

#     choices = schema.Choice(
#         title=u"Independent select",
#         description=u"Do not take part to the decision tree",
#         required=False,
#         vocabulary="plone.app.vocabularies.Disposals")


class ICollection(Interface):
    """Dexterity Schema for ICollection
    """

    title = schema.TextLine(
        title=_(u'Reference No'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    dr_no = schema.TextLine(
        title=_(u'DR No.'),
        required=False,
    )

    # form.widget(table=DataGridFieldFactory)
    # table = schema.List(
    #     title=u"Tree select",
    #     value_type=DictRow(title=u"tablerow", schema=ITableRowSchema),
    #     description=u"Normal decision tree. The last columns are independent of decision tree values."
    #     )


    withholding = schema.Float(
        title=_(u'Withholding'),
        required=False,
    )

    directives.widget('disposals', DataGridFieldFactory, auto_append=False)
    disposals = schema.List(title=_(u'Disposal/Sales'),
                              value_type=DictRow(title=u'Disposal/Sales', schema=ICollectionDisposal),
                              required=True)
    

    collection_type = schema.Choice(
        title=_(u"Collection Type"),
        vocabulary=collection_types,
        required=True,
    )

    check_no = schema.TextLine(
        title=_(u'Check Number'),
        required=False,
    )

    check_due_date = schema.Date(
        title=_(u'Date Check'),
        required=False,
        default=datetime.date.today(),
    )

    directives.widget(office=SingleCheckBoxFieldWidget)
    bounce_check = schema.Bool(
            title=_(u"Bounce Check"),
            required=False,
        )

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )


    @invariant
    def validate_chapters(data):
        start_time = time.time()
        already_added = set()
        client = ''
        disposals = data.disposals
        if not disposals:
            raise Invalid(_(u'Please input a disposal.'))

        for chapter_data in disposals:
            percentage = chapter_data['collection_amount']
            collection_disposal = chapter_data['collection_disposal']

            if (percentage is None) or (collection_disposal is None):
                raise Invalid(_(u'Missing value on disposal.'))

            if collection_disposal:
                chapter = api.content.get(UID=chapter_data['collection_disposal'])
                # percentage must be from 1 to 100
                if 0 >= percentage:
                    raise Invalid(_(u'chapter_percentage_value_1_100',
                        default=u'Chapter percentage value for ${chapter} must not be 0',
                        mapping=dict(chapter=chapter.title)
                    ))
                # prevent multiple assignments of same chapter
                if collection_disposal in already_added:
                    raise Invalid(_(u'duplicate_percentage_participation_chapter',
                        default=u'Duplicate percentage of participation assignment for chapter ${chapter}',
                        mapping=dict(chapter=chapter.title)
                    ))

                # calculate unassigned percentage of participation
                try:
                    local_unassigned_percentage = \
                        data.__context__.get_collection_amount().get(collection_disposal) or 0
                except AttributeError:
                    local_unassigned_percentage = 0
                unassigned_percentage = chapter.get_unassigned_collection_amount() + local_unassigned_percentage
                # prevent assignment of more than the available percentage


                if percentage > unassigned_percentage:
                    raise Invalid(_(u'unassigned_percentage_participation_chapter',
                        default=u'Unassigned percentage of participation for chapter ${chapter} is ${unassigned_percentage}',
                        mapping=dict(chapter=chapter.title, unassigned_percentage=unassigned_percentage)
                    ))
                
                if client and client != chapter.client:
                    raise Invalid(_(u'duplicate_percentage_participation_chapter',
                        default=u'Must only pay for one client. Please check ${chapter}',
                        mapping=dict(chapter=chapter.title)
                    ))

                client = chapter.client
                already_added.add(collection_disposal)
                print "validate chapters {}".format(time.time()- start_time)


class Collection(Item):

    def get_collection_amount(self):
        start_time = time.time()
        data = dict( (row['collection_disposal'], row['collection_amount']) for row in (self.disposals or []))
        print "get_collection_amount {}".format(time.time()- start_time)
        return data

    def get_collection_disposals(self):
        return self.get_collection_amount().keys()

    def set_previous_collection_disposals(self, uids):
        self._v_previous_collection_disposals = uids

    def get_collection_total(self):
        return sum(self.get_collection_amount().values())

    def get_client(self):
        data = self.get_collection_disposals()
        if data:
            return getRelatedObjectInfo(data[0], disposal=True)

    # def get_client_uid(self):
    #     data = self.get_client()
    #     if data:
    #         return data['client']['uid']


@indexer(ICollection)
def collection_disposals(obj):
    return obj.get_collection_disposals()


@indexer(ICollection)
def collection_total(obj):
    return obj.get_collection_total()


@indexer(ICollection)
def client_name(obj):
    return obj.get_client()


def on_collection_add(doc, event):
    start_time = time.time()

    collection_disposals = set(doc.get_collection_disposals())
    collection_disposals |= set(getattr(doc, '_v_previous_collection_disposals', []))
    
    if collection_disposals:
        for b in api.content.find(portal_type='Disposal', UID=list(collection_disposals)):
            print "add collection {}".format(b)
            b.getObject().update_unassigned_collection_amount()
    print "on_collection_add {}".format(time.time()- start_time)


def on_collection_update(doc, event):
    if hasattr(event, 'descriptions') and event.descriptions:
        for d in event.descriptions:
            if d.interface is ICollection and (('disposals' in d.attributes) or ('bounce_check' in d.attributes)):
                on_collection_add(doc, event)


class AddForm(add.DefaultAddForm):
    template = ViewPageTemplateFile('templates/collection_add_form.pt')

    def get_disposal(self):
        request = self.request
        disposal = request.get('disposal')
        if disposal:
            return disposal

    def get_amount(self):
        request = self.request
        amount = request.get('amount')
        if amount:
            return amount

    def get_disposals(self):
        request = self.request
        query = {}

        client = request.get('client')
        if client:
            query['client'] = client

        brains = api.content.find(portal_type='Disposal', 
                            payment_type="TERMS",
                            sort_on="client", **query)
        items = [{'uid': '--NOVALUE--', 'title': 'No value'}]
        if brains is not None:
            for brain in brains:
                title = "{} Php{} Php{}".format(
                    brain.reference_no,
                    brain.disposal_total,
                    brain.unassigned_collection_amount)
                items.append({'uid': brain['UID'],
                            'title': title})

        return json.dumps(items)

    def get_client(self):
        request = self.request
        client = request.get('client')
        if client:
            brains = api.content.find(portal_type='Client', UID=client)
            if brains:
                return brains[0].Title




class AddView(add.DefaultAddView):
    form = AddForm



class EditForm(edit.DefaultEditForm):
    template = ViewPageTemplateFile('templates/collection_edit_form.pt')

    def update(self):
        super(EditForm, self).update()
        editpage = self.context.absolute_url().find('++add++') < 0
        if editpage:
            pass
            # collector = zope.schema.Set(
            #    value_type=zope.schema.Choice(values=(1, 2, 3, 4)),
            #    default=set([1, 3]) )
            # widget = setupWidget(collector)
            # widget.update()


            # import pdb; pdb.set_trace()
            # self.widgets['disposals'].mode = z3c.form.interfaces.DISPLAY_MODE

    def get_disposal(self, uid):
        return getRelatedObjectInfo(uid, disposal=True)

    def get_client(self):
        request = self.request
        client = request.get('client')
        if client:
            brains = api.content.find(portal_type='Client', UID=client)
            if brains:
                return brains[0].Title
