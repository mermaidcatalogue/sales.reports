# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
import datetime

from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

turnover_types = SimpleVocabulary(
    [SimpleTerm(value=u'SAMPLE', title=_(u'Sample')),
     SimpleTerm(value=u'ADJUSTMENT', title=_(u'Adjustment')),]
    )

class IProductsList(Interface):
    product_uid = schema.Choice(title=_(u'Products'),
        vocabulary="list_products",
        required=False)
    kilos = schema.Float(
        title=_(u"Kilos"),
        required=True,
    )

    price = schema.Float(
        title=_(u"Price"),
        required=False,
    )


class ITurnover(Interface):

    reference_no = schema.TextLine(
        title=_(u'DR No.'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    directives.widget('input_products', DataGridFieldFactory)
    input_products = schema.List(title=_(u'Products'),
                              value_type=DictRow(title=u'Products, Kilos and Prices', schema=IProductsList),
                              required=True)

    turnover_type = schema.Choice(
            title=_(u"Turnover Type"),
            vocabulary=turnover_types,
            required=False,
        )

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )

    @invariant
    def validate_turnover(data):
        already_added = set()
        for product in data.input_products:
            product_uid = product['product_uid']
            if product_uid:
                # prevent multiple assignments of same product
                if product_uid in already_added:
                    raise Invalid(_(u'Duplicate product. Please double check.'))

                already_added.add(product_uid)


class Turnover(Item):
    # Make sure Item's accessors don't take precedence
    # Add your class methods and properties here
    def get_object_uids(self):
        return [row['product_uid'] for row in (self.input_products or [])]

    def get_object_kilos(self):
        return dict(
            (row['product_uid'], row['kilos'])
            for row in (self.input_products or [])
        )

    def get_object_price(self):
        return dict(
            (row['product_uid'], row['price'])
            for row in (self.input_products or [])
        )

@indexer(ITurnover)
def product_uids(obj):
    return obj.get_object_uids()

@indexer(ITurnover)
def product_kilos(obj):
    return obj.get_object_kilos()


@indexer(ITurnover)
def product_price(obj):
    return obj.get_object_kilos()

