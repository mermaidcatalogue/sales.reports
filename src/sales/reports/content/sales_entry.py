# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
import datetime


class ISalesInput(Interface):
    product_uid = schema.Choice(title=_(u'Products'),
        vocabulary="list_products",
        required=False)

    kilos = schema.Float(
        title=_(u"Kilos"),
        required=True,
    )

    price = schema.Float(
        title=_(u"U-Cost"),
        required=False,
    )


class ISalesEntry(Interface):
    """Dexterity Schema for ISalesEntry
    """

    title = schema.TextLine(
        title=_(u'Reference No'),
        required=True,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    client = schema.Choice(title=_(u'Client'),
        vocabulary="list_clients",
        required=False)


    amount = schema.Float(
        title=_(u"Amount"),
        required=True,
    )


    # directives.widget('input_products', DataGridFieldFactory)
    # input_products = schema.List(title=_(u'Products'),
    #                           value_type=DictRow(title=u'Products, Kilos and U-Cost', schema=ISalesInput),
    #                           required=True)

    # due_date = schema.Date(
    #     title=_(u'Due Date'),
    #     required=True,
    # )

    # paid = schema.TextLine(
    #     title=_(u'Paid'),
    #     required=True,
    # )

    notes = schema.Text(
        title=_(u'Notes'),
        required=False,
    )