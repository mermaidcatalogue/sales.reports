# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from z3c.form.browser.checkbox import SingleCheckBoxFieldWidget
import datetime


class IExpenseTypeList(Interface):
    expense_location = schema.Choice(title=_(u'Expense Location'),
        vocabulary="list_expense_locations",
        required=False)

    expense_type = schema.Choice(title=_(u'Particular'),
        vocabulary="list_expense_types",
        required=False)
    
    expense_info = schema.TextLine(
        title=_(u"Expense Info"),
        required=True,
    )

    amount = schema.Float(
        title=_(u"Amount"),
        required=True,
    )

class IExpense(Interface):
    """Dexterity Schema for IExpense
    """

    title = schema.TextLine(
        title=_(u'Reference No.'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    # directives.widget('expenses_data', DataGridFieldFactory)
    # expenses_data = schema.List(title=_(u'Expenses'),
    #                           value_type=DictRow(title=u'Particular, Expense Info and Amount', schema=IExpenseTypeList),
    #                           required=True)

    # expense_location = schema.Choice(title=_(u'Expense Location'),
    #     vocabulary="list_expense_locations",
    #     required=True)

    expense_type = schema.Choice(title=_(u'Expense Type'),
        vocabulary="list_expense_types",
        required=True)

    # expense_type_a = schema.Choice(title=_(u'Expense Sub Type'),
    #     vocabulary="list_expense_types_a",
    #     required=False)


    # expense_type_b = schema.Choice(title=_(u'Particular A'),
    #     vocabulary="list_expense_types_b",
    #     required=False)

    # expense_type_c = schema.Choice(title=_(u'Particular B'),
    #     vocabulary="list_expense_types_c",
    #     required=False)
    
    expense_info = schema.TextLine(
        title=_(u"Expense Info"),
        required=False,
    )

    quantity = schema.Float(
        title=_(u"Quantity"),
        required=False,
    )

    price = schema.Float(
        title=_(u"Price"),
        required=True,
    )

    directives.mode(paid='hidden')
    directives.widget(paid=SingleCheckBoxFieldWidget)
    paid = schema.Bool(
            title=_(u"Paid"),
            default=True,
        )

    directives.widget(pin_expense=SingleCheckBoxFieldWidget)
    pin_expense = schema.Bool(
            title=_(u"Pin Expense to Income Statement"),
            default=False,
        )
    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )

