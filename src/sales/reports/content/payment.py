# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from z3c.relationfield.schema import RelationChoice
from z3c.relationfield.schema import RelationList
from sales.reports.content.disposal import IDisposal
from plone import api

from five import grok
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.dexterity.browser import add
from plone.dexterity.browser.edit import DefaultEditForm, DefaultEditView
import z3c.form

from sales.reports.browser.utils import getRelatedObjectInfo
from plone.dexterity.browser import edit
from z3c.form.browser.checkbox import SingleCheckBoxFieldWidget
import datetime


class IPaymentDisposal(Interface):
    payment_disposal = schema.Choice(title=_(u'Client - Reference No. - Total Amount - Running Balance'),
        vocabulary="list_disposals",
        required=False)

    payment_amount = schema.Float(
        title=_(u"Amount"),
        required=False,
    )

class IPayment(Interface):
    """Dexterity Schema for IPayment
    """

    title = schema.TextLine(
        title=_(u'Reference No'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    dr_no = schema.TextLine(
        title=_(u'DR No.'),
        required=False,
    )

    directives.widget('disposals', DataGridFieldFactory, auto_append=False)
    disposals = schema.List(title=_(u'Disposal/Sales'),
                              value_type=DictRow(title=u'Disposal/Sales', schema=IPaymentDisposal),
                              required=True)

    # collector = schema.TextLine(
    #     title=_(u'Collector'),
    #     required=False,
    # )

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )


    @invariant
    def validate_chapters(data):
        already_added = set()
        client = ''
        for chapter_data in data.disposals:
            percentage = chapter_data['payment_amount']
            payment_disposal = chapter_data['payment_disposal']
            if payment_disposal:
                chapter = api.content.get(UID=chapter_data['payment_disposal'])
                # percentage must be from 1 to 100
                if 0 >= percentage:
                    raise Invalid(_(u'chapter_percentage_value_1_100',
                        default=u'Chapter percentage value for ${chapter} must not be 0',
                        mapping=dict(chapter=chapter.title)
                    ))
                # prevent multiple assignments of same chapter
                if payment_disposal in already_added:
                    raise Invalid(_(u'duplicate_percentage_participation_chapter',
                        default=u'Duplicate percentage of participation assignment for chapter ${chapter}',
                        mapping=dict(chapter=chapter.title)
                    ))

                # calculate unassigned percentage of participation
                try:
                    local_unassigned_percentage = \
                        data.__context__.get_payment_amount().get(payment_disposal) or 0
                except AttributeError:
                    local_unassigned_percentage = 0
                unassigned_percentage = chapter.get_unassigned_collection_amount() + local_unassigned_percentage
                # prevent assignment of more than the available percentage


                if percentage > unassigned_percentage:
                    raise Invalid(_(u'unassigned_percentage_participation_chapter',
                        default=u'Unassigned percentage of participation for chapter ${chapter} is ${unassigned_percentage}',
                        mapping=dict(chapter=chapter.title, unassigned_percentage=unassigned_percentage)
                    ))
                
                if client and client != chapter.client:
                    raise Invalid(_(u'duplicate_percentage_participation_chapter',
                        default=u'Must only pay for one client. Please check ${chapter}',
                        mapping=dict(chapter=chapter.title)
                    ))

                client = chapter.client
                already_added.add(payment_disposal)




class Payment(Item):

    def get_payment_disposals(self):
        return [row['payment_disposal'] for row in (self.disposals or [])]

    def get_payment_amount(self):
        return dict(
            (row['payment_disposal'], row['payment_amount'])
            for row in (self.disposals or [])
        )

    def set_previous_payment_disposals(self, uids):
        self._v_previous_payment_disposals = uids

    def get_payment_total(self):
        return sum([p['payment_amount']
            for p in (self.disposals or [])])

    def get_payment_client(self):
        data = self.get_payment_disposals()
        if data:
            return getRelatedObjectInfo(self.get_payment_disposals()[0], disposal=True)


@indexer(IPayment)
def payment_disposals(obj):
    return obj.get_payment_disposals()

@indexer(IPayment)
def chapters_payment_amount(obj):
    return obj.get_payment_amount()


@indexer(IPayment)
def payment_total(obj):
    return obj.get_payment_total()

@indexer(IPayment)
def payment_client(obj):
    return obj.get_payment_client()


def on_payment_add(doc, event):

    payment_disposals = set(doc.get_payment_disposals())
    payment_disposals |= set(getattr(doc, '_v_previous_payment_disposals', []))
    
    if payment_disposals:
        for b in api.content.find(portal_type='Disposal', UID=list(payment_disposals)):
            b.getObject().update_unassigned_payment_amount()


def on_payment_update(doc, event):
    if hasattr(event, 'descriptions') and event.descriptions:
        for d in event.descriptions:
            if d.interface is IPayment and ('disposals' in d.attributes):
                on_payment_add(doc, event)


class AddForm(add.DefaultAddForm):
    template = ViewPageTemplateFile('templates/payment_add_form.pt')

    def get_disposal(self):
        request = self.request
        disposal = request.get('disposal')
        if disposal:
            return disposal

    def get_amount(self):
        request = self.request
        amount = request.get('amount')
        if amount:
            return amount

class AddView(add.DefaultAddView):
    form = AddForm




class EditForm(edit.DefaultEditForm):
    template = ViewPageTemplateFile('templates/payment_edit_form.pt')

    def update(self):
        super(EditForm, self).update()
        editpage = self.context.absolute_url().find('++add++') < 0
        if editpage:
            pass
            # collector = zope.schema.Set(
            #    value_type=zope.schema.Choice(values=(1, 2, 3, 4)),
            #    default=set([1, 3]) )
            # widget = setupWidget(collector)
            # widget.update()


            # import pdb; pdb.set_trace()
            # self.widgets['disposals'].mode = z3c.form.interfaces.DISPLAY_MODE

    def get_disposal(self, uid):
        return getRelatedObjectInfo(uid, disposal=True)
