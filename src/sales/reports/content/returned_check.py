# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
import datetime

class IReturnedCheck(Interface):
    """Dexterity Schema for IReturnedCheck
    """

    title = schema.TextLine(
        title=_(u'Reference No.'),
        required=True,
    )

    client = schema.TextLine(
        title=_(u'Client'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=False,
        default=datetime.date.today(),
    )

    check_no = schema.TextLine(
        title=_(u'Check No.'),
        required=False,
    )

    check_date = schema.Date(
        title=_(u'Check Date'),
        required=False,
    )

    amount = schema.Float(
        title=_(u'Amount'),
        required=False,
    )

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )