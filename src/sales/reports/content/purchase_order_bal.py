# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from z3c.form.browser.checkbox import SingleCheckBoxFieldWidget
from plone import api
from sales.reports.browser.utils import getRelatedObjectInfo
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.dexterity.browser import add
from plone.dexterity.browser.edit import DefaultEditForm, DefaultEditView
from plone.dexterity.browser import edit
from plone.directives import form
import datetime
import operator


class IDeliveries(form.Schema):
    delivery_date = schema.Date(
        title=_(u'Delivery Date'),
        required=False,
    )

    delivery_qty = schema.Float(
        title=_(u"Delivery Qty"),
        required=False,
    )

class IPurchaseOrderBal(Interface):

    title = schema.TextLine(
        title=_(u'Name'),
        required=True,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    particulars = schema.TextLine(
        title=_(u'Particulars'),
        required=False,
    )

    quantity = schema.Float(
        title=_(u'Quantity'),
        required=True,
    )

    date_expected_start = schema.Date(
        title=_(u'Date Expected Start'),
        required=False,
        default=datetime.date.today(),
    )

    date_expected_end = schema.Date(
        title=_(u'Date Expected End'),
        required=False,
        default=datetime.date.today(),
    )

    directives.widget('deliveries', DataGridFieldFactory)
    deliveries = schema.List(title=_(u'Deliveries'),
                              value_type=DictRow(title=u'Delivery Date and Delivery Quantity', schema=IDeliveries),
                              required=True)

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )

    @invariant
    def validate_data(data):
        if data.date_expected_start > data.date_expected_end:
            raise Invalid(_(u'Date expected start must not be ahead of date expected end.'))

        if data.deliveries:
            for d in data.deliveries:
                if not d['delivery_date'] or not d['delivery_qty']:
                    raise Invalid(_(u'Delivery date or delivery quantity missing.'))


class PurchaseOrderBal(Item):
    def get_delivery_qty(self):
        return map(operator.itemgetter('delivery_qty'), self.deliveries)

    def get_delivery_balance(self):
        return self.quantity - sum(self.get_delivery_qty())


@indexer(IPurchaseOrderBal)
def delivery_qty(obj):
    return obj.get_delivery_qty()


@indexer(IPurchaseOrderBal)
def delivery_balance(obj):
    return obj.get_delivery_balance()


