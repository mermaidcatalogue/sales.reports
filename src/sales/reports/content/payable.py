# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from zope.interface import invariant, Invalid
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from z3c.form.browser.checkbox import SingleCheckBoxFieldWidget
import datetime


class IPayable(Interface):
    """Dexterity Schema for IPayable
    """

    title = schema.TextLine(
        title=_(u'Reference No.'),
        required=False,
    )

    date = schema.Date(
        title=_(u'Date'),
        required=True,
        default=datetime.date.today(),
    )

    expense_type = schema.Choice(title=_(u'Payable Type'),
        vocabulary="list_expense_types",
        required=True)

    expense_info = schema.TextLine(
        title=_(u"Payable Info"),
        required=False,
    )

    # expense_type_a = schema.Choice(title=_(u'Payable Sub Type'),
    #     vocabulary="list_expense_types_a",
    #     required=False)


    # expense_type_b = schema.Choice(title=_(u'Particular A'),
    #     vocabulary="list_expense_types_b",
    #     required=False)

    # expense_type_c = schema.Choice(title=_(u'Particular B'),
    #     vocabulary="list_expense_types_c",
    #     required=False)

    quantity = schema.Float(
        title=_(u"Quantity"),
        required=False,
    )

    price = schema.Float(
        title=_(u"Price"),
        required=True,
    )


    directives.widget(paid=SingleCheckBoxFieldWidget)
    paid = schema.Bool(
            title=_(u"Paid"),
            required=False,
        )
    
    directives.widget(pin_expense=SingleCheckBoxFieldWidget)
    pin_expense = schema.Bool(
            title=_(u"Pin to Income Statement"),
            required=False,
        )

    notes = schema.TextLine(
        title=_(u'Notes'),
        required=False,
    )