# -*- coding: utf-8 -*-
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from sales.reports.interfaces import IDisposal
from sales.reports.testing import SALES_REPORTS_INTEGRATION_TESTING  # noqa
from zope.component import createObject
from zope.component import queryUtility

import unittest


class DisposalIntegrationTest(unittest.TestCase):

    layer = SALES_REPORTS_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Disposal')
        schema = fti.lookupSchema()
        self.assertEqual(IDisposal, schema)

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Disposal')
        self.assertTrue(fti)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Disposal')
        factory = fti.factory
        obj = createObject(factory)
        self.assertTrue(IDisposal.providedBy(obj))

    def test_adding(self):
        obj = api.content.create(
            container=self.portal,
            type='Disposal',
            id='Disposal',
        )
        self.assertTrue(IDisposal.providedBy(obj))
