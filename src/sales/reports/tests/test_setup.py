# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from sales.reports.testing import SALES_REPORTS_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that sales.reports is properly installed."""

    layer = SALES_REPORTS_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if sales.reports is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'sales.reports'))

    def test_browserlayer(self):
        """Test that ISalesReportsLayer is registered."""
        from sales.reports.interfaces import (
            ISalesReportsLayer)
        from plone.browserlayer import utils
        self.assertIn(
            ISalesReportsLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = SALES_REPORTS_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['sales.reports'])

    def test_product_uninstalled(self):
        """Test if sales.reports is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'sales.reports'))

    def test_browserlayer_removed(self):
        """Test that ISalesReportsLayer is removed."""
        from sales.reports.interfaces import \
            ISalesReportsLayer
        from plone.browserlayer import utils
        self.assertNotIn(
           ISalesReportsLayer,
           utils.registered_layers())
