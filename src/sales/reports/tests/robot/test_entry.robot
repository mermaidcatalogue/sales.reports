# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s sales.reports -t test_entry.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src sales.reports.testing.SALES_REPORTS_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_entry.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Disposal
  Given a logged-in site administrator
    and an add entry form
   When I type 'My Disposal' into the title field
    and I submit the form
   Then a entry with the title 'My Disposal' has been created

Scenario: As a site administrator I can view a Disposal
  Given a logged-in site administrator
    and a entry 'My Disposal'
   When I go to the entry view
   Then I can see the entry title 'My Disposal'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add entry form
  Go To  ${PLONE_URL}/++add++Disposal

a entry 'My Disposal'
  Create content  type=Disposal  id=my-entry  title=My Disposal


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.title  ${title}

I submit the form
  Click Button  Save

I go to the entry view
  Go To  ${PLONE_URL}/my-entry
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a entry with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the entry title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
