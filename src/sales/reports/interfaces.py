# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sales.reports import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationChoice
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource
from plone.namedfile.field import NamedFile
from sales.reports.validators import isFileTypeCSV
from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives



class ISalesReportsLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""



class IEntries(Interface):
    """Dexterity Schema for IProducts
    """

    title = schema.TextLine(
        title=_(u'Title'),
        required=True,
    )

    description = schema.Text(
        title=_(u'Description'),
        required=False,
    )

    sales_csv = NamedFile(
        title=_(u'CSV file for Disposals'),
        constraint=isFileTypeCSV,
        required=False
    )

    turnover_csv = NamedFile(
        title=_(u'CSV file for Turnovers'),
        constraint=isFileTypeCSV,
        required=False
    )


        


class IProduct(Interface):
    """Dexterity Schema for IProducts
    """

    title = schema.TextLine(
        title=_(u'Product Name'),
        required=True,
    )

    description = schema.Text(
        title=_(u'Description'),
        required=False,
    )

class IProducts(Interface):
    """Dexterity Schema for IProducts
    """

    title = schema.TextLine(
        title=_(u'Product Name'),
        required=True,
    )

    description = schema.Text(
        title=_(u'Description'),
        required=False,
    )

    products_csv = NamedFile(
        title=_(u'CSV file for products'),
        constraint=isFileTypeCSV,
        required=False
    )

