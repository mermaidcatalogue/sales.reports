import json
import operator
from operator import attrgetter, itemgetter
import sys
import datetime
from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import logging
from StringIO import StringIO
import transaction
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
import zope.schema
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
import datetime
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby

class EntriesYearlyChildProxy(ResultProxy):

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

    @Lazy
    def product_uids(self):
        return self.brain.product_uids[self.master.UID]

class InventoryReportMixin(CustomPaginatedView):

    render = ViewPageTemplateFile('templates/report_entries_yearly_view.pt')

    def group_items(self, brains):
        data = ResultSet(brains, EntriesYearlyChildProxy)
        group =  groupby(data, attrgetter("date.month"))
        return self.data(group) 

    @Lazy
    def disposal_items(self):
        year = self.year
        brains = api.content.find(portal_type='Disposal',
                                  sort_on='date',
                                  date = dict(query= [datetime.date(year, 1, 1), 
                                                    datetime.date(year, 12, 31)],
                                                 range='min:max'))
        return self.group_items(brains)

    @Lazy
    def turnover_items(self):
        year = self.year
        brains = api.content.find(portal_type='Turnover', sort_on='date',
                                    date = dict(query= [datetime.date(year, 1, 1), 
                                                        datetime.date(year, 12, 31)], range='min:max'))
        return self.group_items(brains)
        
    def data(self, group):
        result = []
        f = defaultdict(int)
        for month,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'month': month,
                            'data': c,
                            'total': self.get_itervalues_total(c)})

        return result

    @Lazy
    def input_products(self):
        return self.brain.input_products

    @Lazy
    def turnover_items_total(self):
        data = [x['data'] for x in self.turnover_items]
        return self.get_total_items_by_products(data) 
         

    @Lazy
    def turnover_previous(self):
        previous_inventory = self.previous_year_inventory_total
        turnover = self.turnover_items_total
        #combine two dictionaries by keys
        return {x: previous_inventory.get(x, 0) + turnover.get(x, 0) for x in set(previous_inventory).union(turnover)} 


    @Lazy
    def disposal_items_total(self):
        data = [x['data'] for x in self.disposal_items]
        return self.get_total_items_by_products(data)

    @Lazy
    def products_list(self):
        products = set(self.disposal_items_total.keys() + self.turnover_items_total.keys())
        return sorted([{'uuid':p, 'title': self.get_object(p)['title']} for p in products], key=itemgetter('title'))


    @Lazy
    def t_overall_total(self):
        return self.get_itervalues_total(self.turnover_items_total)

    @Lazy
    def d_overall_total(self):
        return self.get_itervalues_total(self.disposal_items_total)

    @Lazy
    def inventory_total(self):
        turnover= Counter(self.turnover_previous)
        disposal = Counter(self.disposal_items_total)
        turnover.subtract(disposal)
        return dict(turnover)

    @Lazy
    def inventory_overall_total(self):
        return self.t_overall_total - self.d_overall_total


    def _get_monitoring_data(self, content_type, year):
        begin_date = datetime.date(year, 1, 1)
        end_date = datetime.date(year, 12, 31)
        brains = api.content.find(portal_type=content_type,
            date=dict(query=[begin_date, end_date], range='min:max'),
        )
        return brains

    @Lazy
    def previous_year_disposal_data(self):
        brains = self._get_monitoring_data('Disposal',self.year-1)
        return self.get_total_items_by_products1(brains)

    @Lazy
    def previous_year_turnover_data(self):
        brains = self._get_monitoring_data('Turnover',self.year-1)
        return self.get_total_items_by_products1(brains)

    def get_total_items_by_products1(self, v):
        result = defaultdict(int)
        brains = ResultSet(v, EntriesYearlyChildProxy)
        data = [k.input_products for k in brains]
        for k in data:
            for p in k:
                if p['product_uid']:
                    result[p['product_uid']] += p['kilos']
        return result


    @Lazy
    def previous_year_inventory_total(self):
        disposal = Counter(self.previous_year_disposal_data)
        turnover = Counter(self.previous_year_turnover_data)
        turnover.subtract(disposal)
        return dict(turnover)

    @Lazy
    def previous_year_overall_total(self):
        return self.get_itervalues_total(self.previous_year_inventory_total)


class InventoryReportView(InventoryReportMixin, SalesReportPDFView):
    
    @property
    def orientation(self):
        """Set orientation to Landscape
        """
        return 'Landscape'


