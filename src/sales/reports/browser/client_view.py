
import json
import operator
import itertools
import sys
import datetime

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import logging
from StringIO import StringIO
import transaction
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
import zope.schema
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
import datetime
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby
import json



class ClientEntryProxy(ResultProxy):

    @property
    def title(self):
        return self.object.title

    @property
    def date(self):
        return self.date_format(self.object.date)

    @property
    def type_data(self):
        obj = self.object
        portal_type = obj.portal_type

        if portal_type == 'Disposal': 
            collector = obj.collector
            if collector:
                collector = "(%s)" %(self.get_object(collector)['title'])

            return "%s - %s %s" % (portal_type, 
                            obj.payment_type.title() , 
                            collector)

        if portal_type == 'Collection':
            collection_type = obj.collection_type
            if collection_type == 'check':
                collection_type = "Check (%s - %s)" % (obj.check_no, self.date_format(obj.check_due_date))

            return "%s - %s" % (portal_type, collection_type.title())

    @property
    def type(self):
        return self.object.portal_type

    @property
    def total(self):
        if self.type == 'Disposal':
            return self.brain.disposal_total

        if self.type == 'Collection':
            return self.brain.collection_total

    @property
    def reference_no(self):
        return self.object.reference_no

    @property
    def info(self):
        if self.type == 'Collection':
            return [self.get_disposal_object(brain) 
                    for brain in self.get_collection_disposals()]

        if self.type == 'Disposal':
            return [{'price': brain['price'],
                    'kilos': brain['kilos'],
                    'product': self.get_object(brain['product_uid'])} 
                    for brain in self.object.input_products]



class ClientView(CustomPaginatedView):
    """ Client view
    """
    render = ViewPageTemplateFile('templates/client_view.pt')

    def get_query(self):
        q = {}
        year = self.year
        q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                             range='min:max')

        return q

    @Lazy
    def items(self):
        client = self.context.UID()
        brains = api.content.find(portal_type=["Disposal", "Collection"], 
                                sort_on='date', 
                                client=client,
                                sort_order='reverse',
                                **self.get_query()) 

        return ResultSet(brains, ClientEntryProxy)


    @Lazy
    def sample(self):
        client = self.context.UID()
        brains = api.content.find(portal_type=["Disposal", "Collection"], 
                                sort_on='date', 
                                client=client,
                                sort_order='reverse',
                                **self.get_query()) 

        data = []
        for brain in brains:
            data.append((brain.UID, brain.Title))

        response = {
            'sEcho': len(data),
            'iTotalRecords': len(data),
            'iTotalDisplayRecords': len(data),
            'aaData': data
        }
        
        return json.dumps(response)



class ajax_view(CustomPaginatedView):
    """ Client view
    """
    render = ViewPageTemplateFile('templates/ajax_view.pt')

    def get_query(self):
        q = {}
        year = self.year
        q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                             range='min:max')

        return q

    @Lazy
    def sample(self):
        self.request.response.setHeader('Content-Type', 'application/json')
        client = self.context.UID()
        brains = api.content.find(portal_type=["Disposal", "Collection"], 
                                sort_on='date', 
                                client=client,
                                sort_order='reverse',
                                **self.get_query()) 

        data = []
        for brain in brains:
            data.append((brain.UID))

        response = {
            'sEcho': len(data),
            'iTotalRecords': len(data),
            'iTotalDisplayRecords': len(data),
            'aaData': data
        }
        
        return json.dumps(response)








