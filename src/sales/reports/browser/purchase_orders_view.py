
import json
import operator
import itertools
import sys
import datetime

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import logging
from StringIO import StringIO
import transaction
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
import zope.schema
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
import datetime
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby


class PurchaseOrdersProxy(ResultProxy):

    @property
    def date(self):
        return self.date_format(self.object.date)

    @property
    def date_expected(self):
    	start = self.date_format(self.date_expected_start)
    	end = self.date_format(self.date_expected_end)
        return '{} - {}'.format(start, end)

    @property
    def delivered(self):
    	dates = map(operator.itemgetter('delivery_date'), self.deliveries)
        return map(self.date_format, dates)


class PurchaseOrdersView(CustomPaginatedView):
    """ PurchaseOrdersView view
    """
    render = ViewPageTemplateFile('templates/purchase_orders_view.pt')

    def get_query(self):
        q = {}
        
        try:
            year = int(self.request.get('year'))
        except (ValueError, TypeError) as e:
            year = self.year

        try:
            month = int(self.request.get('month'))
        except (ValueError, TypeError) as e:
            month = self.month

        last_date_month = self.last_date_month(year, month)
        q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                        range='min:max')


        text = self.request.get('text')
        if text:
            q['SearchableText'] = text
        return q

    @Lazy
    def items(self):
        return ResultSet(api.content.find(self.context, 
                portal_type='PurchaseOrderBal', 
                sort_on='sortable_title', **self.get_query()), PurchaseOrdersProxy)


