import base64
from DateTime import DateTime
import os
import tempfile
from babel import numbers
from subprocess import Popen, PIPE, STDOUT
import zope.schema
from zope.component import getMultiAdapter
from zope.cachedescriptors.property import Lazy
from plone import api
from plone.batching import Batch
from plone.batching.browser import BatchView
from plone.uuid.interfaces import IUUID
from ZTUtils import make_query
from Products.Five.browser import BrowserView
from plone.namedfile.file import NamedFile, NamedBlobImage
from plone.app.vocabularies.catalog import CatalogSource
from sales.reports import _
from plone.api import user
from z3c.relationfield.schema import RelationList
from z3c.relationfield import RelationValue
import datetime
from Products.ZCTextIndex import ParseTree
from collections import defaultdict, Counter
from calendar import monthrange
from sales.reports.browser.utils import getRelatedObjectInfo



class ResultSet(object):
    """Lazily accessed set of objects."""

    def __init__(self, results, wrapper, **extra):
        self.results = results
        self.wrapper = wrapper
        self.extra = extra

    def __len__(self):
        return len(self.results)

    def __iter__(self):
        for res in self.results:
            yield self.wrapper(res, **self.extra)

    def __getslice__(self, *args, **kwargs):
        results = self.results.__getslice__(*args, **kwargs)
        return self.__class__(results, self.wrapper, **self.extra)

    def __getitem__(self, i):
        return self.wrapper(self.results[i], **self.extra)


class ResultProxy(object):
    """ Non persistent proxy object.

    Basically it' just's a place to add custom methods to catalog brains.

    This is where caching will take place later.

    Default implementation just tries to get the values from the
    catalog brain metadata and fallbacks to real object if no such column
    """

    def __init__(self, brain, **extra):
        self.brain = brain
        self.extra = extra

    @Lazy
    def object(self):
        return self.brain.getObject()

    def __getattr__(self, attr):
        try:
            return getattr(self.brain, attr)
        except AttributeError:
            return getattr(self.object, attr)

    def get_object(self, uid = None):
        if uid:
            return getRelatedObjectInfo(uid)

    def get_disposal_object(self, uid = None):
        if uid:
            return getRelatedObjectInfo(uid, disposal=True)

    def date_format(self, date=None):
        if date:
            return date.strftime('%m/%d/%Y')

    def format_decimal(self, number):
        """Return the given decimal number formatted for current language
        """
        if number:
            return format(number, ',')



class SalesBatchView(BatchView):

    def make_link(self, page):
        batchlinkparams = self.request.form.copy()
        return '%s?%s' % (self.request.ACTUAL_URL,
            make_query(batchlinkparams, {'page': page}))


class CustomPaginatedView(BrowserView):
    """ Base paginated view
    """

    @Lazy
    def year(self):
        if self.request.get('year'):
            return int(self.request.get('year'))
        return datetime.datetime.now().year

    @Lazy
    def prev_year(self):
        year = self.year
        return  int(year) -1

    @Lazy
    def date(self):
        if self.request.get('date'):
            return self.request.get('date')


    def date_now(self):
        return datetime.datetime.now().date()

    @Lazy
    def month(self):
        if self.request.get('month'):
            return int(self.request.get('month'))
        return datetime.datetime.now().month

    @Lazy
    def prev_month(self):
        month = self.month
        if month == 1:
            return 12
        return  int(month) -1

    def get_object(self, uid = None):
        if uid:
            return getRelatedObjectInfo(uid)


    def date_format(self, date=None):
        if date:
            return date.strftime('%m/%d/%Y')

    def last_date_month(self, year, month):
        return monthrange(year, month)[1]

    def datetime(self, date=None):
        if date:
            return datetime.datetime.strptime(date, '%Y-%m-%d').date()

    @property
    def board(self):
        """Return board object
        """
        return getParentBoardObject(self.context)

    @Lazy
    def products(self):
        return [
            dict(uuid=b.UID, title=b.Title)
            for b in api.content.find(portal_type='Product', 
                                    sort_on='sortable_title')
            ]

    @Lazy
    def months(self):
        return {1:'January', 2:'February', 3:'March', 4:'April', 5:'May', 6:'June',
                 7:'July', 8:'August', 9:'September', 10:'October', 11:'November', 12:'December'}

    def get_itervalues_total(self, v):
        total = sum(v.itervalues())
        if total < 0:
            return 0.0
        return sum(v.itervalues())

    def get_total_items_by_products(self, v):
        d = defaultdict(int)
        for k in v:
            for f,v in k.items():
                if v < 0:
                    v = 0.0
                d[f]+=v
        return d

    @property
    def items(self):
        raise NotImplementedError

    @Lazy
    def batch(self):
        batch_size = 3
        page = max(0, int(self.request.get('page', '1')) - 1)
        try:
            items = self.items
        except ParseTree.ParseError:
            # SearchableText query may contain invalid search keywords or wildcards
            items = []
        return Batch(items, start=page*batch_size, size=batch_size)

    @Lazy
    def batch20(self):
        batch_size = 20
        page = max(0, int(self.request.get('page', '1')) - 1)
        try:
            items = self.items
        except ParseTree.ParseError:
            # SearchableText query may contain invalid search keywords or wildcards
            items = []
        return Batch(items, start=page*batch_size, size=batch_size)

    @Lazy
    def batchCashDisposal(self):
        batch_size = 15
        page = max(0, int(self.request.get('page', '1')) - 1)
        try:
            items = self.disposal_cash_items
        except ParseTree.ParseError:
            # SearchableText query may contain invalid search keywords or wildcards
            items = []
        return Batch(items, start=page*batch_size, size=batch_size)

    @Lazy
    def batchCash(self):
        batch_size = 15
        page = max(0, int(self.request.get('page', '1')) - 1)
        try:
            items = self.cash
        except ParseTree.ParseError:
            # SearchableText query may contain invalid search keywords or wildcards
            items = []
        return Batch(items, start=page*batch_size, size=batch_size)

    @Lazy
    def batchCheck(self):
        batch_size = 15
        page = max(0, int(self.request.get('page', '1')) - 1)
        try:
            items = self.check
        except ParseTree.ParseError:
            # SearchableText query may contain invalid search keywords or wildcards
            items = []
        return Batch(items, start=page*batch_size, size=batch_size)

    @Lazy
    def batching(self):
        return SalesBatchView(self.context, self.request)(self.batch)

    @property
    def board(self):
        """Return board object
        """
        return self.context


    def format_decimal(self, number, data=None):
        """Return the given decimal number formatted for current language
        """
        if number:
            return format(number, ',')
        if data:
            return number

    @Lazy
    def expense_types(self):
        return [
            dict(uuid=b.UID, title=b.Title)
            for b in api.content.find(portal_type='ExpenseType', sort_on='sortable_title')
        ]

    @Lazy
    def expense_locations(self):
        return [
            dict(uuid=b.UID, title=b.Title)
            for b in api.content.find(portal_type='ExpenseLocation', sort_on='sortable_title')
        ]

    @Lazy
    def clients(self):
        return [
            dict(uuid=b.UID, title=b.Title)
            for b in api.content.find(portal_type='Client', sort_on='sortable_title')
        ]


class SalesCSVView(CustomPaginatedView):
    def get_csv_data(self):
        csv = self.request.get('csv')
        csv_file = getattr(self.context, csv)
        if csv_file:
            return csv_file.data.split('\n')
        return None

    def clean_cols(self, row):
        if len(row.split(';')) > 1:
            return [x.replace('"', '').replace('\r', '') for x in row.split(';')]
        elif len(row.split(',')) > 1:
            return [x.replace('"', '').replace('\r', '') for x in row.split(',')]
        else:
            raise RuntimeError('Sepeartor in CSV must be either comma (,) or semicolon (;)')



class SalesReportPDFView(BrowserView):
    
    def __call__(self):
        """ Return html or convert the html output to pdf.
        """
        if self.request.get('pdf'):
            self.request.response.setHeader('Content-Type', 'application/pdf')
            self.request.response.setHeader('Content-Disposition', 'attachment; filename="%s"' % self.get_filename())
            self.request.response.setHeader('Last-Modified', DateTime.rfc822(DateTime()))
            pdfcontent = self.html_to_pdf()
            self.request.response.setHeader("Content-Length", len(pdfcontent))
            self.request.response.write(pdfcontent)
            return pdfcontent
        else:
            return self.index()
    
    def get_filename(self):
        """ Generate pdf name.
        """
        return datetime.datetime.now().strftime('report-%d-%m-%Y-%H-%M.pdf')
        
    @property
    def orientation(self):
        """Set orientation to Landscape or Portrait (default Portrait)
        """
        return 'Portrait'
    
    def html_to_pdf(self):
        """ Convert the html using wkhtmltopdf.
        """
        html = self.index().encode('utf8')
        with tempfile.NamedTemporaryFile(suffix='.pdf') as F:
            env = os.environ.copy()
            env['DISPLAY'] = ':1'
            cmd = ['wkhtmltopdf', '--footer-right', '[page]/[topage]',
                   '--encoding', 'utf-8', '--page-size', 'A1', '-O', self.orientation, '-q', '-', F.name,
                   ]
            p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=PIPE, env=env)
            res = p.communicate(input=html)
            pdf = F.read()
            return pdf



  

        



