# -*- coding: utf-8 -*-
from plone import api
from plone.app.uuid.utils import uuidToObject


def getRelatedObjectInfo(uuid=None, obj=None, disposal=False, with_uid=False):
        """
        Retrieve object from uuid or RelationValue.to_object 
        and return its title and url.
        """
        result = {}
        if uuid:
            obj = uuidToObject(uuid)            
        if obj:
            result = {'title': obj.title, 
                      'url': obj.absolute_url(),
                      'description': obj.description}

            if disposal:
                result['reference_no'] = getattr(obj, 'reference_no')
                result['date'] = date_format(getattr(obj, 'date'))
                result['client'] = getRelatedObjectInfo(getattr(obj, 'client'), with_uid=True)
                result['dict_total'] = obj.get_disposal_total()

            if with_uid:
                result['uid'] = uuid

        return result


def date_format( date=None):
        if date:
            return date.strftime('%m/%d/%Y')

        