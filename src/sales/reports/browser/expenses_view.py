import json
import operator
import itertools
import sys
import datetime
import datetime
import zope.schema
import logging
import transaction
import collections

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from StringIO import StringIO
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby





class ExpenseTypeProxy(ResultProxy):

    # XXX: optimize

    @property
    def title(self):
        return self.object.title


class ExpenseProxy(ResultProxy):

    # XXX: optimize

    @property
    def title(self):
        return self.object.title

    @property
    def expense_type_title(self):
        return self.get_object(self.object.expense_type)


    @property
    def pin_expense(self):
        return self.object.pin_expense

    @property
    def price_format(self):
        return self.format_decimal(self.object.price)

    @property
    def expense_info(self):
        return self.object.expense_info

    @property
    def amount(self):
        quantity = self.object.quantity
        price = self.object.price
        if quantity:
            price = quantity * price
        return price

    

class ExpensesView(CustomPaginatedView):
    """Expenses view
    """

    render = ViewPageTemplateFile('templates/expenses_view.pt')

    def get_query_expense(self):
        q = {}
        date = self.request.get('date')
        request_url = self.request.getURL()

        if date:
            q['date'] = self.datetime(date)


        else:
            month = self.month
            year = self.year
            
            if 'yearly_view' in request_url:
                month = 0
            
            if month == 0:
                q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                                 range='min:max')
            else:
                last_date_month = self.last_date_month(year, month)
                q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                                     range='min:max')

        # expense_type_a = self.request.get('expense_type_a')
        # if expense_type_a:
        #     q['expense_type_a'] = expense_type_a

        # expense_type_b = self.request.get('expense_type_b')
        # if expense_type_b:
        #     q['expense_type_b'] = expense_type_b

        if 'income-statement' in request_url:
            q['pin_expense'] = True


        expense_type = self.request.get('expense_type')
        if expense_type:
            q['expense_type'] = expense_type

        text = self.request.get('text')
        if text:
            q['SearchableText'] = text

        q['paid'] = True
        return q


    def group_by_keys_expenses(self, iterable, keys):
        key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': self.get_object(key), 'data': self.list_sub_total(list(group))} for key, group in groupby(sorted_iterable, key_func)]

    def group_by_keys_month_expenses(self, iterable, keys):
        key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': list(group)} for key, group in groupby(sorted_iterable, key_func)]

    def list_sub_total(self, data):
        return {'monthly_results': data, 'monthly_total': self.expenses_sub_total_amount(data)}


    def expenses_sub_total_amount(self, data):
        """Return sum of all amount
        """
        if data:
            return sum(map(operator.attrgetter('amount'), data))
        return 0

    @Lazy
    def expenses_items(self):
        return ResultSet(api.content.find(portal_type=['Expense', 'Payable'], sort_on='date', **self.get_query_expense()), ExpenseProxy)

    @Lazy
    def expenses_types(self):
        return ResultSet(api.content.find(portal_type='ExpenseType', sort_on='sortable_title'), ExpenseTypeProxy)

    @Lazy
    def expenses_types_a(self):
        return ResultSet(api.content.find(portal_type='ExpenseTypeA', sort_on='sortable_title'), ExpenseTypeProxy)

    @Lazy
    def expenses_types_b(self):
        return ResultSet(api.content.find(portal_type='ExpenseTypeB', sort_on='sortable_title'), ExpenseTypeProxy)

    @Lazy
    def expenses_total_amount(self):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('amount'), self.expenses_items))

class ExpensesYearlyView(ExpensesView):
    """Expenses view
    """

    render = ViewPageTemplateFile('templates/expenses_yearly_view.pt')

    @Lazy
    def expenses_data(self):
        first = self.group_by_keys_month_expenses(self.expenses_items, ('date.month',))
        second = [{'title': k['title'],
                    'data': self.group_by_keys_expenses(k['data'], ('expense_location', )), 
                    'total': self.expenses_sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title']['title'], 
                'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results

    @Lazy
    def expenses_summary(self):
        cnt=Counter()
        for p in self.expenses_data:
            for d in p['data']:
                try: 
                    cnt[p['title']['title']] += self.expenses_sub_total_amount(d['data'])
                except TypeError:
                    cnt[p['title']] += self.expenses_sub_total_amount(d['data'])
        return cnt


class ExpensesMonthlyView(ExpensesView):
    """Expenses view
    """

    render = ViewPageTemplateFile('templates/expenses_monthly_view.pt')

    @Lazy
    def expenses_data(self):
        first = self.group_by_keys_expenses(self.expenses_items, ('expense_type',))
        second = [{'title': k['title'],
                'data': self.group_by_keys_month_expenses(k['data']['monthly_results'], ('expense_type_a',)),} for k in first]
        return second

    @Lazy
    def expenses_summary(self):
        cnt=Counter()
        for p in self.expenses_data:
            for d in p['data']:
                try: 
                    cnt[p['title']['title']] += self.expenses_sub_total_amount(d['data'])
                except TypeError:
                    cnt[p['title']] += self.expenses_sub_total_amount(d['data'])
        return cnt

class ExpensesYearlyReportMixin(ExpensesYearlyView):

    render = ViewPageTemplateFile('templates/report_expenses_yearly_view.pt')


class ExpensesMonthlyReportMixin(ExpensesMonthlyView):

    render = ViewPageTemplateFile('templates/report_expenses_monthly_view.pt')



class ExpensesYearlyReportView(ExpensesYearlyReportMixin, SalesReportPDFView):
    
    @property
    def orientation(self):
        """Set orientation to Landscape
        """
        return 'Portrait'


class ExpensesMonthlyReportView(ExpensesMonthlyReportMixin, SalesReportPDFView):
    
    @property
    def orientation(self):
        """Set orientation to Landscape
        """
        return 'Portrait'


class ExpenseTypesView(CustomPaginatedView):
    """ ExpenseType view
    """
    render = ViewPageTemplateFile('templates/expense_types_view.pt')

    @Lazy
    def items(self):
        return api.content.find(self.context, portal_type='ExpenseType', sort_on='sortable_title')



class ExpenseLocationsView(CustomPaginatedView):
    """ ExpenseLocation view
    """
    render = ViewPageTemplateFile('templates/expense_locations_view.pt')

    @Lazy
    def items(self):
        return api.content.find(self.context, portal_type='ExpenseLocation', sort_on='sortable_title')




