import json
import operator
import itertools
import sys
import datetime
import datetime
import zope.schema
import logging
import transaction
import collections

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from StringIO import StringIO
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby
from sales.reports.browser.collections_view import CollectionsView
from sales.reports.browser.expenses_view import ExpensesMonthlyView, ExpensesYearlyView



class IncomeStatementView(CollectionsView, ExpensesMonthlyView):
    """IncomeStatementView view
    """

    render = ViewPageTemplateFile('templates/income_statement_view.pt')

    def incoment_statement(self):
    	return self.format_decimal(self.overall_total - self.expenses_total_amount)

  

class IncomeStatementYearlyView(CollectionsView, ExpensesMonthlyView):
    """IncomeStatementView view
    """

    render = ViewPageTemplateFile('templates/income_statement_yearly_view.pt')

    def incoment_statement(self):
    	return self.format_decimal(self.overall_total - self.expenses_total_amount)

  


