
import json
import operator
import itertools
import sys
import datetime

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import logging
from StringIO import StringIO
import transaction
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
import zope.schema
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
import datetime
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby


class ExpenseTypeChildProxy(ResultProxy):

    # XXX: optimize

    @property
    def title(self):
        return self.object.title





class ExpenseTypeProxy(ResultProxy):

    # XXX: optimize

    @property
    def title(self):
        return self.object.title

    # @Lazy
    # def expense_type_a(self):
    #     brains = api.content.find(self.object, portal_type='ExpenseTypeA',
    #                               sort_on='sortable_title')
    #     return ResultSet(brains, ExpenseTypeChildProxy)

    # @Lazy
    # def expense_type_b(self):
    #     brains = api.content.find(self.object, portal_type='ExpenseTypeB',
    #                               sort_on='sortable_title')
    #     return ResultSet(brains, ExpenseTypeChildProxy)


    # @Lazy
    # def expense_type_c(self):
    #     brains = api.content.find(self.object, portal_type='ExpenseTypeC',
    #                               sort_on='sortable_title')
    #    return ResultSet(brains, ExpenseTypeChildProxy)


    

class SettingsView(CustomPaginatedView):
    """ SettingsView 
    """
    render = ViewPageTemplateFile('templates/settings_view.pt')

    @Lazy
    def items(self):
        results = api.content.find(portal_type="ExpenseType", 
                                sort_on='sortable_title',)

        return ResultSet(results, ExpenseTypeProxy)

    @Lazy
    def collectors(self):
        results = api.content.find(portal_type="Collector", 
                                sort_on='sortable_title',)

        return ResultSet(results, ExpenseTypeChildProxy)