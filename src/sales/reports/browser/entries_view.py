
import json
from operator import attrgetter, itemgetter
import itertools
import sys
import datetime




from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import logging
from StringIO import StringIO
import transaction
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
import zope.schema
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
import datetime
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby



class ProductsView(CustomPaginatedView):
    """ Products view
    """
    render = ViewPageTemplateFile('templates/products_view.pt')

    @Lazy
    def items(self):
        return api.content.find(self.context, portal_type='Product', sort_on='sortable_title')


class EntriesChildProxy(ResultProxy):

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

    @Lazy
    def product_uids(self):
        return self.brain.product_uids[self.master.UID]

    @property
    def client_name(self):
        return self.get_object(self.object.client)


class EntriesView(CustomPaginatedView):
    """ Products view
    """
    render = ViewPageTemplateFile('templates/disposals_view.pt')


    def get_query(self):
        q = {}
        year = self.year
        q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                             range='min:max')

        return q

    def items(self, data=None):
        brains = api.content.find(portal_type=data,
                                 sort_on='date',
                                 **self.get_query())
        data = ResultSet(brains, EntriesChildProxy)
        group =  groupby(data, attrgetter("date.month"))
        return self.data(group)

    def data(self, group):
        result = []
        f = defaultdict(int)
        for month,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'month': month,
                            'data': c,
                            'total': self.total_kilos(c)})

        return {'results': result, 'total': f, 'overall_total': self.total_kilos(f)}


    @Lazy
    def inventory_total(self):
        disposal = Counter(self.items('Disposal')['total'])
        turnover = Counter(self.items('Turnover')['total'])
        turnover.subtract(disposal)
        return dict(turnover)

    @Lazy
    def inventory_overall_total(self):
        return self.items('Turnover')['overall_total'] - self.items('Disposal')['overall_total']

    @Lazy
    def input_products(self):
        return self.brain.input_products

    def total_kilos(self, v):
        return sum(v.itervalues())


class DisposalsDateChildProxy(ResultProxy):

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

    @Lazy
    def product_uids(self):
        return self.brain.product_uids[self.master.UID]


class DisposalsDateView(CustomPaginatedView):
    """ Products view
    """
    render = ViewPageTemplateFile('templates/disposals_date_view.pt')

    def get_query(self):
        q = {}
        year = self.year
        month = self.month
        last_date_month = self.last_date_month(year, month)
        q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                             range='min:max')

        return q

    def items(self, data=None):
        brains = api.content.find(portal_type=data,
                                 sort_on='date',
                                 **self.get_query())
        data = ResultSet(brains, DisposalsDateChildProxy)
        group =  groupby(data, attrgetter("date"))
        return self.data(group)

    def data(self, group):
        result = []
        f = defaultdict(int)
        for date,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'date': date,
                            'data': c,
                            'total': self.total_kilos(c)})

        return {'results': result, 'total': f, 'overall_total': self.total_kilos(f)}


    @Lazy
    def inventory_total(self):
        disposal = Counter(self.items('Disposal')['total'])
        turnover = Counter(self.items('Turnover')['total'])
        turnover.subtract(disposal)
        return dict(turnover)

    @Lazy
    def inventory_overall_total(self):
        return self.items('Turnover')['overall_total'] - self.items('Disposal')['overall_total']

    @Lazy
    def input_products(self):
        return self.brain.input_products


    def total_kilos(self, v):
        return sum(v.itervalues())


class TurnoversDateChildProxy(ResultProxy):

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

    @Lazy
    def product_uids(self):
        return self.brain.product_uids[self.master.UID]


class TurnoversDateView(CustomPaginatedView):
    """ Products view
    """
    render = ViewPageTemplateFile('templates/turnovers_date_view.pt')

    def get_query(self):
        q = {}
        year = self.year
        month = self.month
        last_date_month = self.last_date_month(year, month)
        q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                             range='min:max')

        return q

    def items(self, data=None):
        brains = api.content.find(portal_type=data,
                                 sort_on='date',
                                 **self.get_query())
        data = ResultSet(brains, TurnoversDateChildProxy)
        group =  groupby(data, attrgetter("date"))
        return self.data(group)

    def data(self, group):
        result = []
        f = defaultdict(int)
        for date,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'date': date,
                            'data': c,
                            'total': self.total_kilos(c)})

        return {'results': result, 'total': f, 'overall_total': self.total_kilos(f)}


    @Lazy
    def input_products(self):
        return self.brain.input_products

    def total_kilos(self, v):
        return sum(v.itervalues())


class ProductChildProxy(ResultProxy):

    @Lazy
    def master(self):
       return self.extra['master']

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

class ProductProxy(ResultProxy):

    def get_query(self):
        q = {}
        date_start = ''
        date_end = ''
        request = self.extra['request']

        date_end = request.get('date_end')
        if date_end:
            date_end = self.datetime(date_end)

        date_start = request.get('date_start')
        if date_start:
            date_start = self.datetime(date_start)

        if date_start and date_end:
            q['date'] = dict(query=[date_start, date_end], range='min:max')
        q['product_uids']= [self.uid]
        return q

    @Lazy
    def entries(self):
        brains = api.content.find(portal_type='Disposal',
                                    sort_on='date',
                                    sort_order='descending',
                                    **self.get_query())
        return ResultSet(brains, ProductChildProxy, master=self)

    def datetime(self, date=None):
        return datetime.datetime.strptime(date, '%Y-%m-%d').date()

    @Lazy
    def name(self):
        return self.brain.Title

    @Lazy
    def uid(self):
        return self.brain.UID

    @Lazy
    def children(self):
        return list(itertools.chain(self.entries))

    @Lazy
    def kilos(self):
        """Return sum of all kilos from children
        """
        return sum(map(attrgetter('kilos'),
                        self.children))


class ProducstView(CustomPaginatedView):
    """ Products view
    """

    render = ViewPageTemplateFile('templates/disposals_view.pt')

    def get_query(self):
        q = {}
        product_uid = self.request.get('product_uid')
        if product_uid:
            q['UID']= product_uid
        return q

    @Lazy
    def items(self):
        brains = api.content.find(portal_type='Product', **self.get_query())
        return ResultSet(brains, ProductProxy, request=self.request)



class DisposalsProxy(ResultProxy):
    pass


class DisposalsView(CustomPaginatedView):

    render = ViewPageTemplateFile('templates/disposals.pt')

    def get_query(self):
        q = {}
        date = self.request.get('date')
        if date:
            q['date']= self.datetime(date)

        text = self.request.get('text')
        if text:
            q['reference_no']= text
        return q

    @Lazy
    def items(self):
        return ResultSet(api.content.find(self.context, portal_type='Disposal', 
                                                        sort_on='date',
                                                        sort_order='reverse', **self.get_query()), DisposalsProxy)

    # @Lazy
    # def items(self):
    #     brains = api.content.find(self.context, portal_type='Disposal', 
    #                                                     sort_on='date',
    #                                                     sort_order='reverse', **self.get_query())
    #     results = []
    #     for brain in brains:
    #         for product in brain.input_products:
    #             if not product['product_uid']:
    #                 results.append(brain)
    #     return ResultSet(results, DisposalsProxy)


class TurnoversProxy(ResultProxy):
    pass


class Turnovers1View(CustomPaginatedView):

    render = ViewPageTemplateFile('templates/turnovers.pt')

    def get_query(self):
        q = {}
        date = self.request.get('date')
        if date:
            q['date']= self.datetime(date)

        text = self.request.get('text')
        if text:
            q['reference_no']= text
        return q

    @Lazy
    def items(self):
        return ResultSet(api.content.find(self.context, portal_type='Turnover', 
                                                        sort_on='date',
                                                        sort_order='reverse', **self.get_query()), TurnoversProxy)


        return result



class DisposalsReportMixin(CustomPaginatedView):

    render = ViewPageTemplateFile('templates/report_disposals.pt')

    def get_query(self):
        q = {}
        year = self.year
        month = self.month
        last_date_month = self.last_date_month(year, month)
        q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                             range='min:max')

        return q

    def items(self, data=None):
        brains = api.content.find(portal_type=data,
                                 sort_on='date',
                                 **self.get_query())
        data = ResultSet(brains, DisposalsDateChildProxy)
        group =  groupby(data, attrgetter("date"))
        return self.data(group)

    def data(self, group):
        result = []
        f = defaultdict(int)
        for date,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'date': date,
                            'data': c,
                            'total': self.total_kilos(c)})

        return {'results': result, 'total': f, 'overall_total': self.total_kilos(f)}


    @Lazy
    def inventory_total(self):
        disposal = Counter(self.items('Disposal')['total'])
        turnover = Counter(self.items('Turnover')['total'])
        turnover.subtract(disposal)
        return dict(turnover)

    @Lazy
    def inventory_overall_total(self):
        return self.items('Turnover')['overall_total'] - self.items('Disposal')['overall_total']

    @Lazy
    def input_products(self):
        return self.brain.input_products

    def total_kilos(self, v):
        return sum(v.itervalues())


class DisposalsReportView(DisposalsReportMixin, SalesReportPDFView):
    
    @property
    def orientation(self):
        """Set orientation to Landscape
        """
        return 'Landscape'



class TurnoversReportMixin(CustomPaginatedView):

    render = ViewPageTemplateFile('templates/report_turnovers.pt')

    def get_query(self):
        q = {}
        year = self.year
        month = self.month
        last_date_month = self.last_date_month(year, month)
        q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                             range='min:max')

        return q

    def items(self, data=None):
        brains = api.content.find(portal_type=data,
                                 sort_on='date',
                                 **self.get_query())
        data = ResultSet(brains, DisposalsDateChildProxy)
        group =  groupby(data, attrgetter("date"))
        return self.data(group)

    def data(self, group):
        result = []
        f = defaultdict(int)
        for date,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'date': date,
                            'data': c,
                            'total': self.total_kilos(c)})

        return {'results': result, 'total': f, 'overall_total': self.total_kilos(f)}


    @Lazy
    def inventory_total(self):
        disposal = Counter(self.items('Disposal')['total'])
        turnover = Counter(self.items('Turnover')['total'])
        turnover.subtract(disposal)
        return dict(turnover)

    @Lazy
    def inventory_overall_total(self):
        return self.items('Turnover')['overall_total'] - self.items('Disposal')['overall_total']

    @Lazy
    def input_products(self):
        return self.brain.input_products


    def total_kilos(self, v):
        return sum(v.itervalues())

class TurnoversReportView(TurnoversReportMixin, SalesReportPDFView):
    
    @property
    def orientation(self):
        """Set orientation to Landscape
        """
        return 'Landscape'






#yearly


class DisposalsEntryProxy(ResultProxy):

    @property
    def title(self):
        return self.object.title

    @property
    def disposal_total(self):
        return self.brain.disposal_total

    @property
    def total(self):
        return self.disposal_total
    

    @property
    def date_obj(self):
        return self.object.date

    @property
    def date(self):
        return self.date_format(self.date_obj)

    @property
    def type(self):
        return self.object.portal_type

    @property
    def reference_no(self):
        return self.object.reference_no



class EntriesYearlyChildProxy(ResultProxy):

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

    @Lazy
    def product_uids(self):
        return self.brain.product_uids[self.master.UID]


class EntriesYearlyView(CustomPaginatedView):
    """ Products view
    """
    render = ViewPageTemplateFile('templates/entries_yearly_view.pt')

    def group_items(self, brains):
        data = ResultSet(brains, EntriesYearlyChildProxy)
        group =  groupby(data, attrgetter("date.month"))
        return self.data(group) 

    @Lazy
    def disposal_items(self):
        year = self.year
        brains = api.content.find(portal_type='Disposal',
                                  sort_on='date',
                                  date = dict(query= [datetime.date(year, 1, 1), 
                                                    datetime.date(year, 12, 31)],
                                                 range='min:max'))
        return self.group_items(brains)

    @Lazy
    def turnover_items(self):
        year = self.year
        brains = api.content.find(portal_type='Turnover',sort_on='date', 
                                    date = dict(query= [datetime.date(year, 1, 1), 
                                                        datetime.date(year, 12, 31)], range='min:max'))
        return self.group_items(brains)
        
    def data(self, group):
        result = []
        f = defaultdict(int)
        for month,v in group: #month
            c = defaultdict(int)
            for p in v: #disposals
                for m in p.input_products: #input_products
                    if m['product_uid']:
                        c[m['product_uid']] += m['kilos']
                        f[m['product_uid']] += m['kilos']

            result.append({'month': month,
                            'data': c,
                            'total': self.get_itervalues_total(c)})

        return result

    @Lazy
    def input_products(self):
        return self.brain.input_products

    @Lazy
    def turnover_items_total(self):
        data = [x['data'] for x in self.turnover_items]
        return self.get_total_items_by_products(data) 
         

    @Lazy
    def turnover_previous(self):
        previous_inventory = self.previous_year_inventory_total
        turnover = self.turnover_items_total
        #combine two dictionaries by keys
        return {x: previous_inventory.get(x, 0) + turnover.get(x, 0) for x in set(previous_inventory).union(turnover)} 


    @Lazy
    def disposal_items_total(self):
        data = [x['data'] for x in self.disposal_items]
        return self.get_total_items_by_products(data)


    @Lazy
    def t_overall_total(self):
        total = self.get_itervalues_total(self.turnover_items_total)
        if total < 0:
            return 0.0
        return total

    @Lazy
    def d_overall_total(self):
        return self.get_itervalues_total(self.disposal_items_total)

    # @Lazy
    # def products_list(self):
    #     products = set(self.disposal_items_total.keys() + self.turnover_items_total.keys())
    #     return sorted([{'uuid':p, 'title': self.get_object(p)['title']} for p in products], key=itemgetter('title'))

    # @Lazy
    # def products_list(self):
    #     return [
    #         dict(uuid=b.UID, title=b.Title)
    #         for b in api.content.find(portal_type='Product', 
    #                                     sort_on='sortable_title')
    #         ]


    @Lazy
    def products_list(self):
        products = set(self.inventory_total.keys())
        #return [{'uuid':p, 'title': self.get_object(p)['title']} for p in products]
        return sorted([{'uuid':p, 'title': self.get_object(p)['title']} for p in products], key=itemgetter('title'))

    @Lazy
    def divide_products(self):
        max_len = 28
        prod_len = len(self.products_list)
        ans = (prod_len/max_len)+1

        result = []
        for x in xrange(ans):
            y = float(prod_len) / ans



        import pdb; pdb.set_trace()
        return ''

    @Lazy
    def inventory_total(self):
        turnover= Counter(self.turnover_previous)
        disposal = Counter(self.disposal_items_total)
        turnover.subtract(disposal)
        data = dict(turnover)
        for k, v in data.iteritems():
            if v < 0:
                data[k] = 0.0

        return data
        

    @Lazy
    def inventory_overall_total(self):
        return sum(self.inventory_total.values())


    def _get_monitoring_data(self, content_type, year):
        begin_date = datetime.date(year, 1, 1)
        end_date = datetime.date(year, 12, 31)
        brains = api.content.find(portal_type=content_type,
            date=dict(query=[begin_date, end_date], range='min:max'),
        )
        return brains

    @Lazy
    def previous_year_disposal_data(self):
        brains = self._get_monitoring_data('Disposal',self.year-1)
        return self.get_total_items_by_products1(brains)

    @Lazy
    def previous_year_turnover_data(self):
        brains = self._get_monitoring_data('Turnover',self.year-1)
        return self.get_total_items_by_products1(brains)

    def get_total_items_by_products1(self, v):
        result = defaultdict(int)
        brains = ResultSet(v, EntriesYearlyChildProxy)
        data = [k.input_products for k in brains]
        for k in data:
            for p in k:
                if p['product_uid']:
                    if p['kilos'] < 0:
                        p['kilos'] = 0.0
                    result[p['product_uid']] += p['kilos']
        return result

    @Lazy
    def previous_year_inventory_total(self):
        disposal = Counter(self.previous_year_disposal_data)
        turnover = Counter(self.previous_year_turnover_data)
        turnover.subtract(disposal)
        data = dict(turnover)
        for k, v in data.iteritems():
            if v < 0:
                data[k] = 0.0
        return data

    @Lazy
    def previous_year_overall_total(self):
        return self.get_itervalues_total(self.previous_year_inventory_total)






class PhysicalInventoryView(EntriesYearlyView):

    render = ViewPageTemplateFile('templates/physical_inventory_view.pt')

    @Lazy
    def data(self):
        return self.inventory_total




class EntriesYearlyReportViewMixin(EntriesYearlyView):

    render = ViewPageTemplateFile('templates/report_entries_yearly1_view.pt')
    


class EntriesYearlyReportView(EntriesYearlyReportViewMixin, SalesReportPDFView):
    
    @property
    def orientation(self):
        """Set orientation to Landscape
        """
        return 'Landscape'
