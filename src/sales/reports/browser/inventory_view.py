import json
import operator
import itertools
import sys
import datetime
import datetime
import zope.schema
import logging
import transaction
import collections

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from StringIO import StringIO
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby




class CollectionsChildProxy(ResultProxy):
    pass

class InventoryEntryProxy(ResultProxy):

    # XXX: optimize

    @Lazy
    def kilos(self):
        return self.brain.product_kilos[self.master.UID]

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

    @Lazy
    def product_uids(self):
        return self.brain.product_uids[self.master.UID]


class InventoryView(CustomPaginatedView):
    """Collections view
    """

    render = ViewPageTemplateFile('templates/inventory_view.pt')

    def get_query(self):
        q = {}
        date = self.request.get('date')
        if date:
            if date == 'now':
                q['date'] = self.date_now()
            else: 
                q['date'] = self.datetime(date)

        else:
            month = self.month
            year = self.year

            request_url = self.request.getURL()
            if 'monthly_view' not in request_url:
                month = 0
            
            if month == 0:
                q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                                 range='min:max')
            else:
                last_date_month = self.last_date_month(year, month)
                q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                                     range='min:max')


        text = self.request.get('text')
        if text:
            q['SearchableText'] = text

    
        return q

    

    def group_by_keys_month(self, iterable, keys):
        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=keys)
        return [{'title': key, 'data': list(group)} for key, group in groupby(sorted_iterable, keys)]

   
    def list_sub_total(self, data):
        return {'monthly_results': ResultSet(data, CollectionsChildProxy)}


    @Lazy
    def items(self):

        disposal = api.content.find(portal_type="Disposal", 
                                sort_on='date',
                                **self.get_query())


        return ResultSet(disposal, InventoryEntryProxy)



    def group_by_keys(self, iterable, keys):
        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=keys)
        return groupby(sorted_iterable, keys)




class InventoryMonthlyView(InventoryView):
    render = ViewPageTemplateFile('templates/inventory_view.pt')

    @Lazy
    def data(self):
        result = []
        for item in self.items:
            for i in item.input_products:
                i['disposal'] = item.UID
                i['date'] = item.date
                result.append(i)
        return result

    @Lazy
    def group_date_products(self):
        result = defaultdict(list)
        for k,v in self.group_by_keys(self.data,(lambda x:x['date'].month)):
            for d, items in self.group_by_keys(v, operator.itemgetter('product_uid')):
                data = [item['kilos'] for item in items]
                result[k].append({
                    'product_uid' : d,
                    'total' : sum(data),
                })

        return result

    @Lazy
    def products(self):
        products = set([d['product_uid'] for d in self.data])
        return sorted([getRelatedObjectInfo(product, with_uid=True) 
            for product in products], key= operator.itemgetter('title'))













