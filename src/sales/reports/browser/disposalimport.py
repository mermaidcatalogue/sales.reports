from StringIO import StringIO
import csv
import logging
import collections

import transaction
from zope.cachedescriptors.property import Lazy

from plone.batching import Batch
from plone.batching.browser import BatchView
from plone import api

from ZTUtils import make_query
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.statusmessages.interfaces import IStatusMessage

from .common import ResultSet, ResultProxy, CustomPaginatedView

from sales.reports import _
from DateTime import DateTime
import re
import datetime


logger = logging.getLogger(__name__)

def x_float(v):
    if not v:
        return 0.
    try:
        i1 = v.index('.')
    except ValueError:
        i1 = -1
    try:
        i2 = v.index(',')
    except ValueError:
        i2 = -1
    if i1 < i2:
        return float(v.replace('.', '').replace(',', '.'))
    return float(v.replace(',', ''))


def x_text(v):
    if v:
        return v.strip()

def x_date(v):
    try:
        return DateTime(v, "%m/%d/%Y")
    except:
        RuntimeError('invalid date: %s' % v)

def x_array(v):
    try:
        return list(v)
    except:
        RuntimeError('invalid array: %s' % v)


def x_description(v):
    try:
        int(v)
    except ValueError:
        return v
    else:
        raise RuntimeError('invalid description: %s' % v)


class DisposalImportView(BrowserView):

    _csv_field_schema = (
        ('input_products', x_array, 1),
    )

    def __call__(self):
        messages = IStatusMessage(self.context.REQUEST)
        if self.context.REQUEST.get('dry_run_import', '') != '':
            delimiter, new_chapters, updated_chapters, missing_products, duplicate_chapters, skipped_lines = self.run_csv_import()
            transaction.abort()
            messages.add(self.translate("Test performed. Click Start to proceed with the import"), type=u"warning")
        elif self.context.REQUEST.get('run_import', '') != '':
            delimiter,new_chapters, updated_chapters, missing_products, duplicate_chapters, skipped_lines = self.run_csv_import()
        else:
            return self.request.response.redirect(self.context.absolute_url())
        msg = self.translate("Guessed delimiter: ") + unicode(delimiter)
        messages.add(msg, type=u"info")
        if skipped_lines:
            msg = self.translate("Skipped lines: ") + ' ' + ', '.join(str(x) for x in skipped_lines)
            messages.add(msg, type=u"warning")
        if duplicate_chapters:
            msg = self.translate("Duplicates:") + ' ' + ', '.join(duplicate_chapters)
            messages.add(msg, type=u"warning")
        if missing_products:
            msg = self.translate("Missing Product/s: ") + ' ' + ', '.join(missing_products)
            messages.add(msg, type=u"warning")
        if new_chapters:
            msg = self.translate("New:") + ' ' + ', '.join(new_chapters)
            messages.add(msg, type=u"info")

        if updated_chapters:
            msg = self.translate("Updated:") + ' ' + ', '.join(updated_chapters)
            messages.add(msg, type=u"info")
        msg = _("Index values updated.")
        messages.add(msg, type=u"info")
        return self.request.response.redirect(self.context.absolute_url())

    def _get_row_data(self, row):
        row.extend([''] * (12 - len(row)))
        return dict(
            (field, parse(row[col]))
            for (field, parse, col) in self._csv_field_schema
        )

    def translate(self, text):
        return self.context.translate(text, domain='sales.reports')

    def _get_delimtier(self, csv_data):
        rows = csv_data.split('\n')
        for row in rows:
            if len(row.split(';')) > 5:
               return ';'
            elif len(row.split(',')) > 5:
               return ','
        return None


    def clean_string(self, data=None):
        return re.sub('\W+', '', data).lower()

    def get_object(self, product=None):
        result = api.content.find(portal_type='Product', Title=product)
        if result:
            return result[0].UID

    def run_csv_import(self):
        skipped_lines = []
        duplicate_chapters = set()
        delimiter = []
        new_chapters = []
        updated_chapters = []
        missing_products = []
        existing_chapters = dict(
            (b.getObject().date.strftime('%-m/%-d/%y'), b.getObject())
            for b in api.content.find(self.context, portal_type='Disposal')
        )

        print existing_chapters

        existing_products = [self.clean_string(b.Title) for b in api.content.find(portal_type='Product')]

        all_chapters = {}
        log = []
        def update_chapter(chapter, **kwargs):
            # update chapter and reindex
            for k, v in kwargs.items():
                setattr(chapter, k, v)
            chapter.reindexObject()
        #csv_file = self.context.sales_csv
        csv_file = self.context.sales_csv
        if csv_file:
            csv_data = csv_file.data
            # determine delimiter
            delimiter = self._get_delimtier(csv_data)
            if not delimiter:
                raise RuntimeError('Can not determine delimeter of csv file')
            # update disposals
            csv_reader = csv.reader(StringIO(csv_data), delimiter=delimiter)
            csv_products = []
            for i, row in enumerate(csv_reader):
                if i == 1:
                    csv_products = row[1:-1]
                    for r in csv_products:
                        product = self.clean_string(r)
                        if product not in existing_products:
                            missing_products.append(r)

                if '/' in row[0]: #rows with date only
                    code = row[0]
                    input_products = [{'product_uid':self.get_object(csv_products[k]),
                                        'kilos': x_float(v),
                                        'price': 0.0} for k, v in enumerate(row[1:-1]) if v]
                    try:
                        data = self._get_row_data(row)
                    except:
                        skipped_lines.append(i)
                        logger.warning('bad chapter csv line: %r', row)
                    else:
                        if row[0]:
                            try:
                                print (i, row[0])
                                # date = [int(p) for p in row[0].split('/')]
                                # data['date'] = datetime.date(row[2], row[1], row[0])
                                if ('SAMPLE' not in row[0]) and ('ADJUSTMENT' not in row[0]):
                                    date = datetime.datetime.strptime(row[0], '%m/%d/%y').date()
                                else:
                                    entry = row[0].split('-')
                                    date = datetime.datetime.strptime(entry[0], '%m/%d/%y').date()
                                    data['disposal_type'] = entry[1]
                            except:
                                print (i, row[0])
                        data['input_products'] = input_products

                        # skip this line if chapter with the given the code is already imported
                        if code in all_chapters:
                            duplicate_chapters.add(code)
                            continue
                        chapter = existing_chapters.pop(code, None)
                        if chapter:  # existing chapter
                            updated_chapters.append(code)
                        else:  # add new chapter
                            chapter = api.content.create(self.context, 'Disposal', title=code, date=date)
                            new_chapters.append(code)
                        update_chapter(chapter, **data)
                        all_chapters[code] = chapter
                else:
                    skipped_lines.append(i)
                    logger.warning('bad chapter csv line: %r', row)
            #if existing_chapters:  # delete old disposals
                # missing_products = existing_chapters.keys()
                # for code, chapter in existing_chapters.items():
                #     api.content.delete(chapter)
        # update index values
        return (delimiter, new_chapters, updated_chapters, missing_products,
                duplicate_chapters, skipped_lines)





