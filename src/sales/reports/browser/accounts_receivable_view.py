import json
import operator
import itertools
import sys
import datetime
import datetime
import zope.schema
import logging
import transaction
import collections

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from StringIO import StringIO
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby


class AccountsReceivableProxy(ResultProxy):

    # XXX: optimize


    @property
    def uid(self):
        return self.UID

    @property
    def title(self):
        return self.object.title

    @property
    def date_obj(self):
        return self.date_format(self.date)

    @property
    def disposal_total(self):
        return self.brain.disposal_total

    @property
    def unassigned_collection_amount(self):
        return self.brain.unassigned_collection_amount

    @property
    def collector(self):
        return self.get_object(self.object.collector)

    @property
    def input_products(self):
        return [{'price': brain['price'],
                'kilos': brain['kilos'],
                'product': self.get_object(brain['product_uid'])} 
                for brain in self.object.input_products]

        
    

class AccountsReceivableView(CustomPaginatedView):
    """AccountsReceivable view
    """

    render = ViewPageTemplateFile('templates/sales_view.pt')

    def get_query(self):
        q = {}

        reference_no = self.request.get('reference_no')
        if reference_no:
            q['reference_no'] = reference_no

        client = self.request.get('client')
        if client:
            q['client'] = client

        collector = self.request.get('collector')
        if collector:
            q['collector'] = collector

        return q


    def group_by_keys(self, iterable, keys):
        key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': self.list_sub_total(list(group))} for key, group in groupby(sorted_iterable, key_func)]

    def group_by_keys_month(self, iterable, keys):
        key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': list(group)} for key, group in groupby(sorted_iterable, key_func)]

    def list_sub_total(self, data):
        return {'monthly_results': data, 'monthly_total': self.sub_total_amount(data)}


    def sub_total_amount(self, data):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('unassigned_collection_amount'), data))

    @Lazy
    def items(self):
        with_balance = {'query': 1, 'range': 'min'}
        return ResultSet(api.content.find(portal_type='Disposal', 
                                            sort_on='date',
                                            unassigned_collection_amount= with_balance,
                                            payment_type='TERMS',
                                            **self.get_query()), AccountsReceivableProxy)


    @Lazy
    def collectors(self):
        results = api.content.find(portal_type="Collector", 
                                sort_on='sortable_title',)

        return ResultSet(results, AccountsReceivableProxy)


    @Lazy
    def total_amount(self):
        """Return sum of all amount
        """
        return self.format_decimal(sum(map(operator.attrgetter('unassigned_collection_amount'), self.items)))

class AccountsReceivableYearlyView(AccountsReceivableView):
    """AccountsReceivable view
    """

    render = ViewPageTemplateFile('templates/accounts_receivable_yearly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('client',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('payment_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second


class AccountsReceivableMonthlyView(AccountsReceivableView):
    """AccountsReceivable view
    """

    render = ViewPageTemplateFile('templates/accounts_receivable_monthly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys(self.items, ('client',))
        return first

class AccountsReceivableByClientView(AccountsReceivableView):
    """AccountsReceivableByClientView view
    """

    render = ViewPageTemplateFile('templates/accounts_receivable_by_client_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys(self.items, ('client',))
        return first

