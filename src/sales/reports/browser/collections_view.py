import json
import operator
import itertools
import sys
import datetime
import datetime
import zope.schema
import logging
import transaction
import collections

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from StringIO import StringIO
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby


class CollectionsChildProxy(ResultProxy):
    pass

class DiposalEntryProxy(ResultProxy):
    @property
    def title(self):
        return self.object.title

    @property
    def date_obj(self):
        return self.object.date

    @property
    def date(self):
        return self.date_format(self.date_obj)

    @property
    def disposal_total(self):
        return self.brain.disposal_total



class CollectionsEntryProxy(ResultProxy):

    # XXX: optimize

    @property
    def title(self):
        return self.object.title

    @property
    def collection_total(self):
        return self.brain.collection_total

    @property
    def date_obj(self):
        return self.object.date

    @property
    def date(self):
        return self.date_format(self.date_obj)

    @property
    def type(self):
        return self.object.content_type

    @property
    def check_no(self):
        return self.object.check_no

    @property
    def withholding(self):
        withholding =  self.object.withholding
        if withholding:
            return withholding
        return 0

    @property
    def collection_total_with_withholding(self):
        return self.collection_total - self.withholding
    
    @property
    def check_date_format(self):
        return self.date_format(self.object.check_due_date)

    @property
    def reference_no(self):
        return self.object.reference_no

    @property
    def disposals(self):
        return [{'collection_amount': brain['collection_amount'],
                'collection_disposal': self.get_disposal_object(brain['collection_disposal'])} 
                for brain in self.object.disposals]
    

class CollectionsView(CustomPaginatedView):
    """Collections view
    """

    render = ViewPageTemplateFile('templates/collections_view.pt')

    def get_query(self):
        q = {}
        date = self.request.get('date')
        if date:
            if date == 'now':
                q['date'] = self.date_now()
            else: 
                q['date'] = self.datetime(date)

        else:
            month = self.month
            year = self.year

            request_url = self.request.getURL()
            if ('yearly_view' in request_url):
                month = 0
            
            if month == 0:
                q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                                 range='min:max')
            else:
                last_date_month = self.last_date_month(year, month)
                q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                                     range='min:max')


        text = self.request.get('text')
        if text:
            q['SearchableText'] = text

    
        return q


    def group_by_keys(self, iterable, keys):
        key_func = operator.attrgetter(*keys)
        
        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': self.list_sub_total(list(group))} for key, group in groupby(sorted_iterable, key_func)]

    def group_by_keys_month(self, iterable, keys=None, key=None):
        if key:
            key_func = key
        else:
            key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': list(group)} for key, group in groupby(sorted_iterable, key_func)]

    def list_sub_total(self, data):
        return {'monthly_results': ResultSet(data, CollectionsChildProxy), 'monthly_total': self.sub_total_amount(data)}


    def sub_total_amount(self, data):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('collection_total_with_withholding'), data))


    @Lazy
    def items(self):

        cash = api.content.find(portal_type="Collection", 
                                sort_on='date',
                                sort_order='descending',
                                collection_type="cash", 
                                **self.get_query())

        check = api.content.find(portal_type="Collection", 
                                sort_on='date',
                                sort_order='descending',
                                collection_type="check", 
                                bounce_check=False,
                                date = self.get_query()['date']
                                # check_due_date = self.get_query()['date']
                                )

        return ResultSet(cash + check, CollectionsEntryProxy)

    @Lazy
    def check(self):
        check = api.content.find(portal_type="Collection", 
                                sort_on='date',
                                sort_order='descending',
                                collection_type="check", 
                                bounce_check=False,
                                date = self.get_query()['date']
                                # check_due_date = self.get_query()['date']
                                )
        return ResultSet(check, CollectionsEntryProxy)
 

    @Lazy
    def bad_accounts(self):
        check = api.content.find(portal_type="Collection", 
                                sort_on='date',
                                sort_order='descending',
                                collection_type="check", 
                                bounce_check=True,
                                # check_due_date = self.get_query()['date']
                                ) 

        return ResultSet(check, CollectionsEntryProxy)

    @Lazy
    def disposal_cash_items(self):
        return ResultSet(api.content.find(portal_type='Disposal', 
                                            sort_on='date',
                                            sort_order='descending',
                                            payment_type='COD',
                                            **self.get_query()), DiposalEntryProxy)

    @Lazy
    def check_month(self):
        #self.group_by_keys_month(self.check, key =lambda x:x.date.split('/')[0])
        # first = self.group_by_keys_month(self.check, ('date.month',))
        first = self.group_by_keys_month(self.check, key =lambda x:x.date.split('/')[0])
        second = [{'title': k['title'], 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def total_withholding(self):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('withholding'), self.items))

    @Lazy
    def total_amount_less_wh(self):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('collection_total'), self.items)) - self.total_withholding


    @Lazy
    def total_amount_bad_accounts(self):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('collection_total_with_withholding'), self.bad_accounts))


    @Lazy
    def total_disposal_cash_amount(self):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('disposal_total'), self.disposal_cash_items))



    @Lazy
    def overall_total(self):
        return self.total_amount_less_wh + self.total_disposal_cash_amount




class CollectionsYearlyView(CollectionsView):
    """Collections view
    """

    render = ViewPageTemplateFile('templates/collections_yearly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('date_obj.month',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('collection_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results



class CollectionsMonthlyView(CollectionsView):
    """CollectionsMonthlyView view
    """

    render = ViewPageTemplateFile('templates/collections_monthly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('date',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('collection_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results


class CollectionsDailyView(CollectionsView):
    """CollectionsDailyView view
    """

    render = ViewPageTemplateFile('templates/collections_daily_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('date',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('collection_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def cash(self):
        data = self.data
        if data:
            result = [d for d in data if d['title'] == 'cash']
            if result:
                return result[0]['data']['monthly_results']
        return []

    @Lazy
    def check(self):
        data = self.data
        if data:
            result = [d for d in data if d['title'] == 'check']
            if result:
                return result[0]['data']['monthly_results']
        return []


    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results


class CollectionsMonthlyDateView(CollectionsView):
    """CollectionsMonthlyDateView view
    """

    render = ViewPageTemplateFile('templates/collections_daily_monthly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys(self.items, ('collection_type',))
        return first

    @Lazy
    def cash(self):
        data = self.data
        if data:
            result = [d for d in data if d['title'] == 'cash']
            if result:
                return result[0]['data']['monthly_results']
        return []

    @Lazy
    def check(self):
        data = self.data
        if data:
            result = [d for d in data if d['title'] == 'check']
            if result:
                return result[0]['data']['monthly_results']
        return []

    @Lazy
    def cash_total(self):
        return sum(map(operator.attrgetter('collection_total'), self.cash))

    @Lazy
    def cash_withholding_total(self):
        return sum(map(operator.attrgetter('withholding'), self.cash))

    @Lazy
    def check_total(self):
        return sum(map(operator.attrgetter('collection_total'), self.check))

    @Lazy
    def check_withholding_total(self):
        return sum(map(operator.attrgetter('withholding'), self.check))

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results


class CollectionsBadAccountsView(CollectionsView):

    render = ViewPageTemplateFile('templates/collections_bad_accounts_view.pt')




