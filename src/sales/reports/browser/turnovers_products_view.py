
import json
import operator
import itertools
import sys
import datetime

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import logging
from StringIO import StringIO
import transaction
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
import zope.schema
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
import datetime
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby
from calendar import monthrange


class ProductChildProxy(ResultProxy):

    @Lazy
    def master(self):
       return self.extra['master']

    @Lazy
    def kilos(self):
        kilos = self.brain.product_kilos[self.master.UID]
        if kilos < 0:
            return 0.0
        return kilos

    @Lazy
    def price(self):
        return self.brain.product_price[self.master.UID]

class ProductProxy(ResultProxy):

    def get_query(self):
        q = {}
        date_start = ''
        date_end = ''
        request = self.extra['request']

        month = int(request.get('month'))
        year = int(request.get('year'))
        last_date_month = monthrange(year, month)[1]

        if year:
            q['date'] = dict(query=[datetime.date(2014, 1, 1),  datetime.date(year, month, last_date_month)], range='min:max')
        q['product_uids']= [self.uid]
        return q

    @Lazy
    def disposal(self):
        brains = api.content.find(portal_type='Disposal',
                                    sort_on='date',
                                    sort_order='descending',
                                    **self.get_query())
        return ResultSet(brains, ProductChildProxy, master=self)

    @Lazy
    def turnover(self):
        brains = api.content.find(portal_type='Turnover',
                                    sort_on='date',
                                    sort_order='descending',
                                    **self.get_query())
        return ResultSet(brains, ProductChildProxy, master=self)

    def datetime(self, date=None):
        return datetime.datetime.strptime(date, '%Y-%m-%d').date()

    @Lazy
    def name(self):
        return self.brain.Title

    @Lazy
    def uid(self):
        return self.brain.UID

    @Lazy
    def disposal_children(self):
        return list(itertools.chain(self.disposal))

    @Lazy
    def turnover_children(self):
        return list(itertools.chain(self.turnover))

    @Lazy
    def kilos(self):
        """Return sum of all kilos from children
        """
        turnover = sum(map(operator.attrgetter('kilos'), self.turnover_children)) 
        disposal = sum(map(operator.attrgetter('kilos'), self.disposal_children))
        if turnover < 0:
            turnover = 0.0
        if disposal < 0:
            disposal = 0.0

        total = turnover - disposal
        if total < 0:
            return 0.0
        return total


class ProducstView(CustomPaginatedView):
    """ Products view
    """

    render = ViewPageTemplateFile('templates/turnovers_products_view.pt')

    def get_query(self):
        q = {}
        product_uid = self.request.get('product_uid')
        if product_uid:
            q['UID']= product_uid
        return q

    @Lazy
    def items(self):
        brains = api.content.find(portal_type='Product', sort_on='sortable_title', **self.get_query())
        return ResultSet(brains, ProductProxy, request=self.request)

    @Lazy
    def products(self):
        return [
            dict(uuid=b.UID, title=b.Title)
            for b in api.content.find(portal_type='Product', 
                                        sort_on='sortable_title')
            ]

    @Lazy
    def total_items(self):
        total = sum([item.kilos for item in self.items if item.kilos > 0]) 
        if total:
            if total < 0:
                return 0.0
            return self.format_decimal(total)



