import json
import operator
import itertools
import sys
import datetime
import datetime
import zope.schema
import logging
import transaction
import collections

from zope.cachedescriptors.property import Lazy
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from StringIO import StringIO
from plone.api.content import copy, move
from plone.api.exc import InvalidParameterError
from zExceptions import BadRequest
from plone import api
from .common import ResultSet, ResultProxy, CustomPaginatedView, Batch, SalesBatchView, SalesReportPDFView
from plone.namedfile.file import NamedFile
from sales.reports import _
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from plone.app.uuid.utils import uuidToObject
from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.content import ContentHistoryView
from sales.reports.content.disposal import IDisposal
from plone.memoize import ram
from sales.reports.browser.utils import getRelatedObjectInfo
from collections import defaultdict, Counter
from itertools import groupby


class SalesEntryProxy(ResultProxy):

    # XXX: optimize

    @property
    def title(self):
        return self.object.title

    @property
    def disposal_total(self):
        return self.brain.disposal_total

    @property
    def date(self):
        return self.object.date

    @property
    def date_obj(self):
        return self.date_format(self.date)

    @property
    def client_name(self):
        return self.get_object(self.object.client)

    @property
    def reference_no(self):
        return self.object.reference_no

    @property
    def input_products(self):
        return [{'price': brain['price'],
                'kilos': brain['kilos'],
                'product': self.get_object(brain['product_uid'])} 
                for brain in self.object.input_products]
    

class SalesView(CustomPaginatedView):
    """Sales view
    """

    render = ViewPageTemplateFile('templates/sales_view.pt')

    def get_query(self):
        q = {}
        date = self.request.get('date')
        if date:
            q['date'] = self.datetime(date)

        else:
            month = self.month
            year = self.year

            request_url = self.request.getURL()
            if 'monthly_view' not in request_url:
                month = 0
            
            if month == 0:
                q['date'] = dict(query= [datetime.date(year, 1, 1), datetime.date(year, 12, 31)],
                                 range='min:max')
            else:
                last_date_month = self.last_date_month(year, month)
                q['date'] = dict(query= [datetime.date(year, month, 1), datetime.date(year, month, last_date_month)],
                                     range='min:max')


        text = self.request.get('text')
        if text:
            q['SearchableText'] = text

    
        return q


    def group_by_keys(self, iterable, keys):
        key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': self.list_sub_total(list(group))} for key, group in groupby(sorted_iterable, key_func)]

    def group_by_keys_month(self, iterable, keys):
        key_func = operator.attrgetter(*keys)

        # For groupby() to do what we want, the iterable needs to be sorted
        # by the same key function that we're grouping by.
        sorted_iterable = sorted(iterable, key=key_func)
        return [{'title': key, 'data': list(group)} for key, group in groupby(sorted_iterable, key_func)]

    def list_sub_total(self, data):
        return {'monthly_results': data, 'monthly_total': self.sub_total_amount(data)}


    def sub_total_amount(self, data):
        """Return sum of all amount
        """
        return sum(map(operator.attrgetter('disposal_total'), data))

    @Lazy
    def items(self):
        return ResultSet(api.content.find(portal_type='Disposal', 
                                            sort_on='date',
                                            sort_order='descending',
                                            payment_type=['COD','TERMS'],
                                            **self.get_query()), SalesEntryProxy)

    @Lazy
    def total_amount(self):
        """Return sum of all amount
        """
        return self.format_decimal(sum(map(operator.attrgetter('disposal_total'), self.items)))

class SalesYearlyView(SalesView):
    """Sales view
    """

    render = ViewPageTemplateFile('templates/sales_yearly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('date.month',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('payment_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results



class SalesMonthlyView(SalesView):
    """SalesMonthlyView view
    """

    render = ViewPageTemplateFile('templates/sales_monthly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('date',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('payment_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results


class SalesDailyView(SalesView):
    """SalesDailyView view
    """

    render = ViewPageTemplateFile('templates/sales_daily_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys_month(self.items, ('date',))
        second = [{'title': k['title'], 'data': self.group_by_keys(k['data'], ('payment_type', )), 'total': self.sub_total_amount(k['data'])} for k in first]
        return second

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results


class SalesMonthlyDateView(SalesView):
    """SalesMonthlyDateView view
    """

    render = ViewPageTemplateFile('templates/sales_daily_monthly_view.pt')

    @Lazy
    def data(self):
        first = self.group_by_keys(self.items, ('payment_type',))
        return first

    @Lazy
    def cash(self):
        data = self.data
        if data:
            result = [d for d in data if d['title'] == 'COD']
            if result:
                return result[0]['data']['monthly_results']
        return []

    @Lazy
    def check(self):
        data = self.data
        if data:
            result = [d for d in data if d['title'] == 'TERMS']
            if result:
                return result[0]['data']['monthly_results']
        return []

    @Lazy
    def column_sub_total(self):
        results = collections.Counter()
        data = [{'title':d['title'], 'amount': d['data']['monthly_total']} for brain in self.data for d in brain['data']]
        for sd in data: 
            results[sd['title']]+= int(sd['amount'])
        return results



